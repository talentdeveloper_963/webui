import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LoadingIndicatorService } from '../../services/loading-indicator.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';


@Injectable()
export class BaseRequestBehavior {
  public isBusy: BehaviorSubject<boolean>;

  private showIndicator: boolean;
  private showIndicatorDelay = 1000;
  private hideIndicatorDelay = 500;

  constructor(
    private loadingIndicatorService: LoadingIndicatorService,
  ) {
    this.isBusy = new BehaviorSubject(false);
  }

  public launched(withIndicator: boolean): void {
    if (withIndicator) {
      Observable.timer(this.showIndicatorDelay).subscribe(() => {
        if (this.isBusy.value) {
          this.showIndicator = true;
          this.loadingIndicatorService.show();
        }
      });
    }
    this.isBusy.next(true);

  }

  public completed(): void {
    if (this.showIndicator) {
      Observable.timer(this.hideIndicatorDelay).subscribe(() => {
        this.showIndicator = false;
        this.loadingIndicatorService.hide();
      });
    }
    this.isBusy.next(false);
  }
}
