import * as gulp from 'gulp';
import Config from '../../config';
import { join } from 'path';

export = () => {
  return gulp.src(join(Config.TMP_DIR, 'oauth/*'))
    .pipe(gulp.dest(join(Config.APP_DEST, 'oauth')));
};
