import { Component, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MenuItem } from 'primeng/primeng';
import { IMAGES } from '../../../constants/images';


@Component({
  moduleId: module.id,
  selector: 'cd-submenu-panel',
  templateUrl: 'submenu-panel.component.html',
  styleUrls: ['submenu-panel.component.css'],
  animations: [
    trigger('submenu', [
      state('hidden', style({
        height: '0px'
      })),
      state('visible', style({
        height: '*'
      })),
      transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
      transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class SubmenuPanelComponent {

  @Input() images = false;

  @Input() item: MenuItem;

  @Input() expanded: boolean;

  defaultImagePath: string = IMAGES.DEFAULT_IMAGE_URL;

  handleClick(event: Event, item: any) {
    if (item.disabled) {
      event.preventDefault();
      return;
    }

    item.expanded = !item.expanded;

    if (!item.url) {
      event.preventDefault();
    }

    if (item.command) {
      item.command({
        originalEvent: event,
        item: item
      });
    }
  }
}
