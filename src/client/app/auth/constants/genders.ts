export const GENDERS = [
  {
    text: 'Male',
    value: 'm'
  },
  {
    text: 'Female',
    value: 'f'
  }
];
