import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class BadApiResponse extends BaseDto {
  private _service: BadServiceSection;
  private _payload: any;

  /**
   * Initialize a new instance of BadApiResponse
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.service = input['service'] ? new BadServiceSection(input['service']) : null;
      this.payload = input['payload'];
  }

  /**
   * Completely clone this instance.
   * @returns BadApiResponse - Cloned object.
   **/
  public clone(): BadApiResponse {
    return new BadApiResponse(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['service'] = this.service ? this.service.toJSON() : null;
    result['payload'] = this.payload;
    return result;
  }

  /**
  * Get value of service
  * @returns - Current value of service.
  **/
  get service(): BadServiceSection {
    return this._service;
  }

  /**
  * Change the value of service.
  * @param newVal - New value to assign.
  **/
  set service(newVal: BadServiceSection) {
    this._service = newVal;
  }

  /**
  * Response payload
  * @returns - Current value of payload.
  **/
  get payload(): any {
    return this._payload;
  }

  /**
  * Change the value of payload.
  * @param newVal - New value to assign.
  **/
  set payload(newVal: any) {
    this._payload = newVal;
  }


}

import { BadServiceSection } from './badservicesection';
