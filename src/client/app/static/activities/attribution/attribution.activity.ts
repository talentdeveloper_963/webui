import { Component } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';
import { ATTRIBUTIONS } from '../../../shared/constants/constants';

export class Icon {
  name: string;
  author: {
    name: string;
    url: string;
  };
  site: {
    name: string;
    url: string;
    displayUrl: string;
  };
  license: {
    shortName: string;
    fullName: string;
    url: string;
  };
}

@Component({
  moduleId: module.id,
  selector: 'cd-attribution',
  templateUrl: 'attribution.activity.html',
  styleUrls: ['attribution.activity.css'],
})
export class AttributionActivityComponent extends BaseStaticComponent {
  icons: Icon[] = [
    {
      name: 'College Graduation',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Theatre Masks',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Tree silhouette',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Family',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Runner silhouette running fast',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Red cross',
      author: {
        url: 'https://www.flaticon.com/authors/good-ware',
        name: 'Good Ware'
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Shop',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Add calendar symbol for events',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Light bulb',
      author: {
        url: 'https://www.flaticon.com/authors/gregor-cresnar',
        name: 'Gregor Cresnar',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Elections',
      author: {
        url: 'https://www.flaticon.com/authors/pixel-perfect',
        name: 'Pixel Perfect',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Share',
      author: {
        url: 'https://www.flaticon.com/authors/chanut',
        name: 'Chanut',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Placeholder',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Back arrow',
      author: {
        url: 'https://www.flaticon.com/authors/becris',
        name: 'Becris',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Analytics',
      author: {
        url: 'https://www.flaticon.com/authors/smashicons',
        name: 'Smashicons',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Support',
      author: {
        url: 'https://www.flaticon.com/authors/good-ware',
        name: 'Good Ware',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Gear',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Trophy',
      author: {
        url: 'https://www.flaticon.com/authors/vectors-market',
        name: 'Vectors Market',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Edit',
      author: {
        url: 'https://www.flaticon.com/authors/hanan',
        name: 'Hanan',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Bank building',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Man silhouette',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Correct Symbol',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Forward arrow',
      author: {
        url: 'https://www.flaticon.com/authors/google',
        name: 'Google',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Back arrow',
      author: {
        url: 'https://www.flaticon.com/authors/google',
        name: 'Google',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Remove Symbol',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Man User',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Bell',
      author: ATTRIBUTIONS.AUTHORS.FREEPIK,
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Padlock Unlock',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Magnifier',
      author: {
        url: 'https://www.flaticon.com/authors/simpleicon',
        name: 'SimpleIcon',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Sign out option',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Plus black symbol',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Minus symbol',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Chevron arrow up',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
    {
      name: 'Chevron arrow down',
      author: {
        url: 'https://www.flaticon.com/authors/dave-gandy',
        name: 'Dave Gandy',
      },
      site: ATTRIBUTIONS.SITES.FLATICON,
      license: ATTRIBUTIONS.LICENSES.CC3
    },
  ];

  constructor(
    translate: TranslateService,
    sessionService: SessionService,
  ) {
    super(translate, sessionService);
  }
}
