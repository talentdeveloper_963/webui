import { Component, OnDestroy } from '@angular/core';
import { AbstractProjectViewComponent } from '../../../../../../shared/components/project-view/project-view.component';
import { Message } from '../../../../../../shared/api/models/message';
import { LocationService } from '../../../location/location.service';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { IdeaService } from '../../services/idea.service';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { SessionService } from '../../../../../../shared/services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { ProjectTypeService } from '../../../project/project-type.service';

@Component({
  moduleId: module.id,
  selector: 'cd-inspire-view',
  templateUrl: 'inspire-view.activity.html'
})
export class InspireViewActivityComponent extends AbstractProjectViewComponent<Message> implements OnDestroy {

  public LABELS = CONSTANTS.INSPIRE.VIEW;

  constructor(
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected inspireService: IdeaService,
    protected locationService: LocationService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected promoteThemService: PromoteThemService,
    protected stateService: StateService,
    protected navigationService: NavigationService,
    protected sessionService: SessionService,
    protected projectTypeService: ProjectTypeService
  ) {
    super(
      inspireService,
      locationService,
      loadingIndicatorService,
      router,
      route,
      promoteThemService,
      stateService,
      navigationService,
      sessionService,
      translate,
      notificationService,
      projectTypeService
    );
  }

  public removeProject() {
    super.removeProject('message');
  }

  public nextProject() {
    super.nextProject(PROJECT_TYPES.IDEA);
  }

  public isVisible() {
    return super.isVisible();
  }

  public onHide() {
    super.onHide();
  }

  public onShow() {
    super.onShow();
  }
}
