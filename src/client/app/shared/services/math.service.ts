import { Injectable } from '@angular/core';

@Injectable()
export class MathService {
  public ceil10(input: number): number {
    let output = 0;
    input = Math.ceil(input);
    output = Math.ceil((input + 1) / 10) * 10;
    return output;
  }

  public floor10(input: number): number {
    let output = 0;
    input = Math.floor(input);
    output = Math.floor((input - 1) / 10) * 10;
    return output;
  }
}
