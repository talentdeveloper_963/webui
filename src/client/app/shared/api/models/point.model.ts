import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class Point extends BaseDto {
  private _latitude: number;
  private _longitude: number;

  /**
   * Initialize a new instance of Point
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
  constructor(input?: any) {
    super();
    // Skip if no input
    if (!input) {
      return;
    }
    input = super.fromCamelCaseToSnakeCase(input);
    this.latitude = input['latitude'];
    this.longitude = input['longitude'];
  }


  /**
   * Completely clone this instance.
   * @returns Point - Cloned object.
   **/
  public clone(): Point {
    return new Point(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['latitude'] = this.latitude;
    result['longitude'] = this.longitude;
    return result;
  }

  /**
   * Get value of latitude
   * @returns - Current value of latitude.
   **/
  get latitude(): number {
    return this._latitude;
  }

  /**
   * Change the value of latitude.
   * @param newVal - New value to assign.
   **/
  set latitude(newVal: number) {
    this._latitude = newVal;
  }

  /**
   * Get value of longitude
   * @returns - Current value of longitude.
   **/
  get longitude(): number {
    return this._longitude;
  }

  /**
   * Change the value of longitude.
   * @param newVal - New value to assign.
   **/
  set longitude(newVal: number) {
    this._longitude = newVal;
  }


}

