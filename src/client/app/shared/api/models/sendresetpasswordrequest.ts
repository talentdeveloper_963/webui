import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class SendResetPasswordRequest extends BaseDto {
  private _email: string;

  /**
   * Initialize a new instance of SendResetPasswordRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.email = input['email'];
  }

  /**
   * Completely clone this instance.
   * @returns SendResetPasswordRequest - Cloned object.
   **/
  public clone(): SendResetPasswordRequest {
    return new SendResetPasswordRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['email'] = this.email;
    return result;
  }

  /**
  * Get value of email
  * @returns - Current value of email.
  **/
  get email(): string {
    return this._email;
  }

  /**
  * Change the value of email.
  * @param newVal - New value to assign.
  **/
  set email(newVal: string) {
    this._email = newVal.trim().toLowerCase();
  }


}

