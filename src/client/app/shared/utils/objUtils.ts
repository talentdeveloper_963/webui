export function fromSnakeCaseToCamelCase(input: any) {
  const output: any = {};
  for (const key in input) {
    if (input.hasOwnProperty(key)) {
      const newKey: string = key.replace(/(_\w)/g, function ($1) {
        return $1[1].toUpperCase();
      });
      output[newKey] = input[key];
    }
  }
  return output;
}

export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
