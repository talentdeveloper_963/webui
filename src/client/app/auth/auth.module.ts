import { LoginFormComponent } from './components/login-form/login-form.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { RegisterActivityComponent } from './activities/register/register.activity';
import { LoginActivityComponent } from './activities/login/login.activity';
import { LoggingService } from '../shared/modules/utility/service/logging.service';
import { BaseHandleApiErrors } from '../shared/api/common/BaseHandleApiErrors';
import { AuthService } from './services/auth.service';
import { AuthApi } from '../shared/api/service/auth';
import { SessionService } from '../shared/services/session.service';
import { GuestGuard } from '../shared/guards/guest.guard';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { TranslateModule } from '@ngx-translate/core';
import { ForgotPasswordActivityComponent } from './activities/forgot-password/forgot-password.activity';
import { ForgotPasswordFormComponent } from './components/forgot-password-form/forgot-password-form.component';
import { ResetPasswordActivityComponent } from './activities/reset-password/reset-password.activity';
import { ResetPasswordFormComponent } from './components/reset-password-form/reset-password-form.component';
import { SocialButtonsComponent } from './components/social-buttons/social-buttons.component';
import { TwitterLoginProvider } from './services/twitter.login.provider';

const routes: Routes = [
  {path: 'login', component: LoginActivityComponent, canActivate: [GuestGuard]},
  {path: 'register', component: RegisterActivityComponent, canActivate: [GuestGuard]},
  {path: 'forgot', component: ForgotPasswordActivityComponent, canActivate: [GuestGuard]},
  {path: 'reset/:token', component: ResetPasswordActivityComponent, canActivate: [GuestGuard]},
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    NgxMyDatePickerModule.forRoot(),
  ],
  declarations: [
    LoginActivityComponent,
    RegisterActivityComponent,
    LoginFormComponent,
    RegisterFormComponent,
    ForgotPasswordActivityComponent,
    ForgotPasswordFormComponent,
    ResetPasswordActivityComponent,
    ResetPasswordFormComponent,
    SocialButtonsComponent,
  ],
  providers: [
    AuthService,
    AuthApi,
    LoggingService,
    BaseHandleApiErrors,
    SessionService,
    GuestGuard,
    TwitterLoginProvider
  ]
})
export class AuthModule {
}
