import { LocationInfo } from '../api/models/location-info.models';
import { Point } from '../api/models/point.model';
import { Camera } from '../models/camera';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Location } from '../api/models/location.models';


export abstract class NavigationService {
  protected clickLocked: boolean;

  protected canSelectLocation = true;

  protected cameraChangedSource: Subject<Camera> = new Subject<Camera>();

  public cameraChanged$: Observable<Camera> = this.cameraChangedSource.asObservable();

  protected locationSelectedSource: Subject<string> = new Subject<string>();

  public locationSelected$: Observable<string> = this.locationSelectedSource.asObservable();

  protected locationCreatedSource: Subject<Point> = new Subject<Point>();

  public locationCreated$: Observable<Point> = this.locationCreatedSource.asObservable();

  protected clickHandler: (e: any) => void;

  protected mapPitchValueSource: Subject<number> = new Subject<number>();

  public mapPitchValue$: Observable<number> = this.mapPitchValueSource.asObservable();

  public abstract init(mapContainer: HTMLElement): boolean;

  public abstract addLocationMarkers(locations: Array<LocationInfo>): void;

  public abstract addLocationMarker(location: Location, type?: string): void;

  public abstract removeAllLocationMarkers(): void;

  public abstract removeLocationMarker(id: string): void;

  public abstract pitchMap(pitch: number): void;

  public abstract zoomIn(): boolean;

  public abstract zoomOut(): boolean;

  public abstract freezeCamera(): void;

  public abstract unfreezeCamera(): void;

  public abstract windowPositionToCartographic(position: object): boolean;

  public abstract windowPositionToCartesian(position: object): boolean;

  public abstract pickObject(position: object): boolean;

  public abstract orientationByPositionAndRotation(position: object, rotation: number): boolean;

  public abstract flyTo(params: object): void;

  public abstract getCurrentCameraCoordinates(): any;

  public abstract enableCameraRotation(enable: boolean): boolean;

  public abstract enableCameraZoom(enable: boolean): boolean;

  public abstract flyToByCoordinates(camera: Camera, onComplete?: () => void): void;

  public abstract setLocation(locationId?: string): void;

  public abstract getCameraPosition(): Camera;

  public abstract setClickHandler(customClickHandler?: (e: any) => void): void;

  public abstract removeClickHandler(): void;

  public abstract lockClick(): void;

  public abstract unlockClick(): void;

  public abstract allowLocationSelection(): void;

  public abstract forbidLocationSelection(): void;

  protected abstract defaultClickHandler(e: any): void;
}
