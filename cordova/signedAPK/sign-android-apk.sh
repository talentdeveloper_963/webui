#!/bin/bash -l
######################################################

ERROR_CODE=0;

# Color Schema

DEFAULT="\e[0m";
WHITE="\e[0m";
WHITE_BOLD="\e[1m";
BLUE="\e[34m";
GREEN="\e[32m";
RED="\e[31m";
YELLOW_REGULAR="\e[33;21m";

function __newLine() {
   echo #Newline;
}

function __exit() {
   echo -e $DEFAULT; # Reset to Default color schema
   exit $ERROR_CODE;
}

function __welcome() {
   echo -e "$WHITE====================================";
   echo -e "$BLUE   Start Sign Android Application   ";
   echo -e "$WHITE====================================";
   __newLine;
}

function __echoDoneStep() {
   echo -e "$GREEN    Done. $DEFAULT";
   __newLine;
}

function __exitWithErrorCode() {
   echo -e "$RED Exit with error code: "$ERROR_CODE;
   __newLine;
   __exit;
}

function __checkErrors() {
   ERROR_CODE=$?;
   if [ $ERROR_CODE = 0 ]
      then __echoDoneStep;
      else __exitWithErrorCode;
   fi;
}

function __clearErrorCode() {
   ERROR_CODE=0;
}

function __clearOldSignedAPK() {
   __clearErrorCode;

   echo -e "$WHITE_BOLD 1.$YELLOW_REGULAR Remove old signed APK ... $DEFAULT";
   __newLine;

   rm -r ./android/ && mkdir ./android || mkdir ./android;
   __checkErrors;
}

function __copyUnsignedAPK() {
   echo -e "$WHITE_BOLD 2.$YELLOW_REGULAR Copy new unsigned APK ... $DEFAULT";
   __newLine;

   cp ../platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ./android/codemos-release-unsigned.apk;
   __checkErrors;
}

function __signAPK() {
   echo -e "$WHITE_BOLD 3.$YELLOW_REGULAR Sign APK ... $DEFAULT";
   __newLine;

   jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore codemos_key.jks ./android/codemos-release-unsigned.apk release;
   __checkErrors;
}

function __zipAPK() {
   echo -e "$WHITE_BOLD 4.$YELLOW_REGULAR Zip APK ... $DEFAULT";
   __newLine;

   zipalign -v 4 ./android/codemos-release-unsigned.apk ./android/codemos.apk;
   rm -f ./android/codemos-release-unsigned.apk
   __checkErrors;
}

###############################################
# RUN SECTION                                 #
###############################################

__welcome;
__clearOldSignedAPK;
__copyUnsignedAPK;
__signAPK;
__zipAPK;
__exit;
