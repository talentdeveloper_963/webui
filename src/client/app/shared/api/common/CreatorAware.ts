import { Author } from '../models/author';

export interface CreatorAware {
  creator: Author;
}
