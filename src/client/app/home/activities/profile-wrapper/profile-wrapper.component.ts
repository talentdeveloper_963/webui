import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-profile-wrapper',
  templateUrl: 'profile-wrapper.component.html',
})
export class ProfileWrapperComponent implements OnInit, OnDestroy {
  public isOwnProfile: boolean;
  private paramSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private sessionService: SessionService,
  ) {}

  ngOnInit() {
    if (this.sessionService.isAuthorized()) {
      this.paramSubscription = this.route.params.subscribe((params: any) => {
        if (this.sessionService.getSecurityPrincipal().getPrincipalId() === params.id) {
          this.isOwnProfile = true;
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
