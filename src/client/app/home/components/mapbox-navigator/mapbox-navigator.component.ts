import { Component, ElementRef, Inject, NgZone, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { LocationService } from '../side-menu/location/location.service';
import { Location } from '../../../shared/api/models/location.models';
import { ActivatedRoute, Router } from '@angular/router';
import { PromoteThemService } from '../side-menu/promote/promote-them/promote-them.service';
import { ViewModeService } from '../../../shared/services/view-mode.service';
import { Point } from '../../../shared/api/models/point.model';
import { RedirectService } from '../../../shared/services/redirect.service';
import { NavigationService } from '../../../shared/map/navigation.service';
import { LocationInfo } from '../../../shared/api/models/location-info.models';
import { ROUTES } from '../../../shared/constants/routes';
import { LoggingService } from '../../../shared/modules/utility/service/logging.service';
import { Camera } from '../../../shared/models/camera';
import { I18N_ERROR_MESSAGES } from '../../../shared/i18n/elements';
import { NotificationService } from '../../../shared/components/notification/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { Distance } from '../../../shared/constants/distance';
import { Subscription } from 'rxjs/Subscription';
import { PROJECT_TYPES } from '../../../shared/constants/projectTypes';


@Component({
  moduleId: module.id,
  selector: 'cd-mapbox-navigator',
  templateUrl: 'mapbox-navigator.component.html',
  styleUrls: ['mapbox-navigator.component.css'],
})
export class MapboxNavigatorComponent implements OnInit, OnDestroy {
  private cameraSubscription: Subscription;
  private locationSelectedSubscription: Subscription;
  private locationCreatedSubscription: Subscription;
  private lastCameraPos: Camera;
  private minCoordinateDifference = 0.01;
  private minZoomDifference = 0.2;

  constructor(@Inject(DOCUMENT) private document: any,
              private elemRef: ElementRef,
              private router: Router,
              private logger: LoggingService,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private notificationService: NotificationService,
              private navigationService: NavigationService,
              private locationService: LocationService,
              private redirectService: RedirectService,
              public viewModeService: ViewModeService,
              public promoteThemService: PromoteThemService,
              private zone: NgZone) {
  }

  public renderLocationMarkers(camera: Camera) {
    if (camera.zoom < Distance.DEFAULT_ZOOM) {
      this.navigationService.removeAllLocationMarkers();
    } else {
      this.locationService.getLocationsInArea(camera.toArea()).subscribe((locations: Array<LocationInfo>) => {
        this.navigationService.addLocationMarkers(locations);
      }, (error) => {
        this.logger.error(error);
      });
    }
  }

  public ngOnInit() {
    this.showHintNotifications();

    const element = this.document.createElement('div');
    element.className = 'map-container';
    this.elemRef.nativeElement.appendChild(element);
    this.navigationService.init(element);
    this.lastCameraPos = this.navigationService.getCameraPosition();
    this.renderLocationMarkers(this.lastCameraPos);




    this.cameraSubscription = this.navigationService.cameraChanged$.subscribe((cameraPosition: Camera) => {
      if (this.hasCameraPositionChangedEnough(cameraPosition)) {
        this.renderLocationMarkers(cameraPosition);
        this.lastCameraPos = cameraPosition;
      }
    });

    this.locationSelectedSubscription = this.navigationService.locationSelected$.subscribe((locationId?: string) => {
      if (locationId) {
        this.zone.run(() => {
          const camera = this.navigationService.getCameraPosition();
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
            ROUTES.location,
            locationId
          ]);
        });
      } else {
        this.navigationService.unfreezeCamera();
        const camera = this.navigationService.getCameraPosition();
        if (camera) {
          this.router.navigate([
            ROUTES.camera,
            Camera.toFormatted(camera.latitude),
            Camera.toFormatted(camera.longitude),
            Camera.toFormatted(camera.zoom),
          ]);
        }
      }
    });

    this.locationCreatedSubscription = this.navigationService.locationCreated$.subscribe((point: Point) => {
      this.locationService.addLocation(point).subscribe(
        (location: Location) => {

          if (this.router.url.indexOf(ROUTES.project) > -1) {
            const type = this.route.snapshot.queryParams.type;
            this.navigationService.addLocationMarker(location, type);
            const camera = this.navigationService.getCameraPosition();
            this.router.navigate([
              ROUTES.camera,
              Camera.toFormatted(camera.latitude),
              Camera.toFormatted(camera.longitude),
              Camera.toFormatted(camera.zoom),
              ROUTES.location,
              location.id,
              ROUTES.project,
            ], {
              queryParams: {
                type,
              }
            });
          } else if (this.router.url.indexOf(ROUTES.event) > -1) {
            this.navigationService.addLocationMarker(location, PROJECT_TYPES.EVENT);
            const camera = this.navigationService.getCameraPosition();
            this.router.navigate([
              ROUTES.camera,
              Camera.toFormatted(camera.latitude),
              Camera.toFormatted(camera.longitude),
              Camera.toFormatted(camera.zoom),
              ROUTES.location,
              location.id,
              ROUTES.event,
            ]);
          } else if (this.router.url.indexOf(ROUTES.inspire) > -1) {
            this.navigationService.addLocationMarker(location, PROJECT_TYPES.IDEA);
            const camera = this.navigationService.getCameraPosition();
            this.router.navigate([
              ROUTES.camera,
              Camera.toFormatted(camera.latitude),
              Camera.toFormatted(camera.longitude),
              Camera.toFormatted(camera.zoom),
              ROUTES.location,
              location.id,
              ROUTES.inspire,
            ]);
          } else {
            this.navigationService.addLocationMarker(location);
            this.navigationService.setLocation(location.id);
          }
        },
        (error) => {
          this.logger.error(error);
          this.translate.get(I18N_ERROR_MESSAGES.unresolvedLocation).subscribe(res => this.notificationService.show(res));
        },
        () => this.navigationService.unlockClick());
    });

  }

  public ngOnDestroy() {
    this.locationSelectedSubscription.unsubscribe();
    this.locationCreatedSubscription.unsubscribe();
    this.cameraSubscription.unsubscribe();
  }

  public showHintNotifications() {
    // TODO Don't flame on this, made right before prod release, will refactor
    this.notificationService.show(I18N_ERROR_MESSAGES.viewHint, null, 3500);
    setTimeout(() => {

      this.notificationService.show(I18N_ERROR_MESSAGES.createHint, null, 4000);
      setTimeout(() => {
        this.notificationService.show(I18N_ERROR_MESSAGES.zoomHint, null, 7000);
      }, 4500);

    }, 4000);
  }

  private hasCameraPositionChangedEnough(newCameraPosition: Camera): boolean {
    return this.lastCameraPos && (
       Math.abs(Math.abs(this.lastCameraPos.latitude) - Math.abs(newCameraPosition.latitude)) > this.minCoordinateDifference
    || Math.abs(Math.abs(this.lastCameraPos.longitude) - Math.abs(newCameraPosition.longitude)) > this.minCoordinateDifference
    || Math.abs(Math.abs(this.lastCameraPos.zoom) - Math.abs(newCameraPosition.zoom)) > this.minZoomDifference);
  }
}
