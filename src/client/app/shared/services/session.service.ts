import { Injectable } from '@angular/core';
import { User } from '../api/models/user';
import { SecurityPrincipal, UserPrincipal } from '../models/SecurityPrincipal';
import { getSessionStorage } from '../utils/getSessionStorage';
import { LanguageService } from './language.service';
import { Subject } from 'rxjs/Subject';
import { Dictionary } from '../types';

class SessionPersistenceState {
  securityPrincipal: SecurityPrincipal;
  // Extra options like preferences (language, etc) comes here

  constructor(sessionPersistenceState: any) {
    this.securityPrincipal = sessionPersistenceState && sessionPersistenceState.securityPrincipal ? new UserPrincipal(sessionPersistenceState.securityPrincipal) : null;
  }

  toJSON(): Dictionary<any> {
    return {
      securityPrincipal: this.securityPrincipal,
    };
  }

}

@Injectable()
export class SessionService {
  private sessionStoreKey = 'session';
  private sessionStore = getSessionStorage();
  private sessionState: SessionPersistenceState;
  private $securityPrincipal: Subject<SecurityPrincipal> = new Subject<SecurityPrincipal>();

  constructor(
    private languageService: LanguageService,
  ) {
    this.restoreSession();
    if (!this.sessionState) {
      this.initializeSession();
    }
  }

  public getSecurityPrincipal(): SecurityPrincipal {
    return this.sessionState.securityPrincipal;
  }

  public getSecurityPrincipalSubject(): Subject<SecurityPrincipal> {
    return this.$securityPrincipal;
  }

  public isAuthorized(): boolean {
    return !!this.sessionState.securityPrincipal;
  }

  public authorize(user: User): void {
    const userPrincipal = new UserPrincipal({user: user});
    this.languageService.setLanguage(user.language);
    this.setSecurityPrincipal(userPrincipal);
  }

  public setSecurityPrincipal(securityPrincipal: SecurityPrincipal): void {
    this.sessionState.securityPrincipal = new UserPrincipal(securityPrincipal);
    this.persistSession();
    this.$securityPrincipal.next(this.sessionState.securityPrincipal);
  }

  public dropSession(): void {
    this.sessionState.securityPrincipal = null;
    this.persistSession();
  }

  private persistSession() {
    const data = JSON.stringify(this.sessionState);
    this.sessionStore.setItem(this.sessionStoreKey, data);
  }

  private initializeSession() {
    this.sessionState = new SessionPersistenceState(null);
  }

  private restoreSession() {
    const data = this.sessionStore.getItem(this.sessionStoreKey);
    if (data) {
      this.initializeSession();
      this.sessionState = new SessionPersistenceState(JSON.parse(data));
    }
  }

}
