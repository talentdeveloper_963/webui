import { Component } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-partners',
  templateUrl: 'partners.activity.html',
  styleUrls: ['partners.activity.css'],
})
export class PartnersActivityComponent extends BaseStaticComponent {
  constructor(
    translate: TranslateService,
    sessionService: SessionService,
  ) {
    super(translate, sessionService);
  }
}
