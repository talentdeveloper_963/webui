import { Component, OnInit } from '@angular/core';
import { GENDERS } from '../../../auth/constants/genders';
import { User } from '../../../shared/api/models/user';
import { Project } from '../../../shared/api/models/project.model';
import { VALIDATIONS } from '../../../auth/constants/validations';
import { ROUTES } from '../../../shared/constants/routes';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UserPrincipal, SecurityPrincipal } from '../../../shared/models/SecurityPrincipal';
import { LANGUAGES } from '../../../auth/constants/languages';
import { SessionService } from '../../../shared/services/session.service';
import { Event } from '../../../shared/api/models/event.model';
import { Message } from '../../../shared/api/models/message';
import { PROJECT_TYPES } from '../../../shared/constants/projectTypes';
import { NavigationService } from '../../../shared/map/navigation.service';
import { Camera } from '../../../shared/models/camera';

@Component({
  moduleId: module.id,
  selector: 'cd-profile-view',
  templateUrl: 'profile-view.activity.html',
  styleUrls: ['profile-view.activity.css'],
})
export class ProfileViewActivityComponent implements OnInit {
  public securityPrincipal: SecurityPrincipal = null;
  public projects: Project[] = [];
  public events: Event[] = [];
  public messages: Message[] = [];
  public VALIDATIONS = VALIDATIONS;
  public ROUTES = ROUTES;
  public userId: string;
  public showProfileDialog = true;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private sessionService: SessionService,
    private navigationService: NavigationService,
    private router: Router
  ) {}

  public ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params.id;

      this.userService.getUser(this.userId).subscribe((user: User) => {
        this.securityPrincipal = new UserPrincipal({user: user});
        this.getProjects();
      });
    });
  }

  public getProjects(projectType?: string): void {
    // TODO Think about how we can do this better
    if (projectType === PROJECT_TYPES.PUBLIC_PROJECT) {
      this.userService.getProjectsById(this.userId).subscribe(projects => {
        this.projects = projects;
      });
    } else if (projectType === PROJECT_TYPES.EVENT) {
      this.userService.getEventsById(this.userId).subscribe(events => {
        this.events = events;
      });
    } else if (projectType === PROJECT_TYPES.IDEA) {
      this.userService.getMessagesById(this.userId).subscribe(messages => {
        this.messages = messages;
      });
    } else {
      this.userService.getProjectsById(this.userId).subscribe(projects => {
        this.projects = projects;
      });

      this.userService.getEventsById(this.userId).subscribe(events => {
        this.events = events;
      });

      this.userService.getMessagesById(this.userId).subscribe(messages => {
        this.messages = messages;
      });
    }
  }

  public onShow() {
    this.showProfileDialog = true;
  }

  public onHide() {
    this.showProfileDialog = false;
    const camera = this.navigationService.getCameraPosition();
    this.router.navigate([
      ROUTES.camera,
      Camera.toFormatted(camera.latitude),
      Camera.toFormatted(camera.longitude),
      Camera.toFormatted(camera.zoom),
    ]);
  }
}
