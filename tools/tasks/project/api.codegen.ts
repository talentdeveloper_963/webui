import * as gulp from 'gulp';
import { join } from 'path';
import Config from '../../config';
import CodegenUtils from '../../utils/project/codegen_utils';
const hbs = require('handlebars');
const codegen = require('swagger-codegen');
const debug = require('debug')('gulp-swagger-codegen:gulpTask');
const defaults = require('defaults-deep');
const gutil = require('gulp-util');
const through = require('through2');
const yaml = require('yamljs');

const codegenTask = (codegenOptions: any) => {
  return through.obj(function streamProcessor(
    file: any,
    encoding: any,
    callback: any
  ) {
    debug('Processing gulp stream file: %s', file.path);

    // Use a gutil stream to write output
    const fileWriter = (path: string, content: any) => {
      debug('  Pushing output file: %s', path);
      this.push(
        new gutil.File({
          path,
          contents: new Buffer(content)
        })
      );
    };

    // Load the YAML file for the swagger model
    debug('Parsing swaggerfile (YAML) -> %s', file.path);
    const fileContent = file.contents.toString(encoding);
    const model = yaml.parse(fileContent);

    // Perform the code generation
    codegen(
      defaults(codegenOptions || {}, { swagger: model, output: fileWriter })
    );
    callback();
  });
};

export = () => {
  const API_CODEGEN_DIR = join(Config.TOOLS_DIR, 'api_codegen');
  const TEMPLATES_DIR = join(API_CODEGEN_DIR, 'templates');
  const SPEC_FILE_NAME = 'swagger.yaml';

  return gulp
    .src(SPEC_FILE_NAME)
    .pipe(
      codegenTask({
        definitionMapper: CodegenUtils.mapDefinitionsFromModel,
        helpers: {
          /* Assign handlebars helpers with this syntax.
         someName: yourHelperFunction
         someName will become the #someName helper in the files.
         */
          // interfaceName: CodegenUtils.resolveInterfaceName,
          propertyName: CodegenUtils.resolvePropertyName,
          className: CodegenUtils.resolveClassName,
          tsType: CodegenUtils.resolveTypescriptType,
          apiMethodName: CodegenUtils.operationNameForController,
          fileNameForClass: CodegenUtils.fileNameForClass,
          tagName: CodegenUtils.resolveTagName
        },
        perDefinition: {
          [`${TEMPLATES_DIR}/model.hbs`]: {
            target: 'models',
            extension: '.ts',
            modelsImportPath: './'
          }
        },
        perPath: {
          [`${TEMPLATES_DIR}/api.service.hbs`]: {
            target: 'service',
            groupBy: 'x-controller',
            extension: '.ts',
            operations: ['get', 'put', 'post', 'delete', 'patch'],
            modelsImportPath: '../models/'
          }
        }
      })
    )
    .pipe(
      gulp.dest(join('src', Config.APP_CLIENT, Config.COMMON_MODULE_DIR, 'api'))
    );
};
