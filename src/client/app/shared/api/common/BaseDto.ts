import { Dictionary } from '../../types';

export abstract class BaseDto {
  /**
   * Convert the current instance to a plain object
   * for serialization purposes.
   * @returns {object} - Plain object.
   **/
  public abstract toJSON(): Dictionary<any>;

  /**
   *
   * @param input
   * @returns {any}
   */
  public fromCamelCaseToSnakeCase(input: any) {
    const output: any = {};
    for (const key in input) {
      if (input.hasOwnProperty(key)) {
        let newKey: string = key.replace(/([A-Z])/g, function($1) {
          return '_' + $1.toLowerCase();
        });
        newKey = newKey.replace(/^_/, '');
        output[newKey] = input[key];
      }
    }
    return output;
  }
}
