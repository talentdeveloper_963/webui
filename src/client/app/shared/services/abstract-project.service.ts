import { Observable } from 'rxjs/Observable';
import { AbstractProject } from '../api/common/AbstractProject';
import { AbstractSaveProjectRequest } from '../api/common/AbstractSaveProjectRequest';

export interface AbstractProjectService<T extends AbstractProject> {

  getProject(id: string): Observable<T>;

  getMyProjects(): Observable<T[]>;

  addProject(project: AbstractSaveProjectRequest): Observable<T>;

  updateProject(id: string, saveProject: AbstractSaveProjectRequest): Observable<T>;

  removeProject(id: string): Observable<any>;

  submitToVote(id: string): Observable<T>;

  vote(id: string): Observable<T>;
}
