import { Injectable, Injector, NgZone } from '@angular/core';
import { LoginCredentialsRequest } from '../../shared/api/models/logincredentialsrequest';
import { UserSignupRequest } from '../../shared/api/models/usersignuprequest';
import { Observable } from 'rxjs/Observable';
import { User } from '../../shared/api/models/user';
import { AuthApi } from '../../shared/api/service/auth';
import { SessionService } from '../../shared/services/session.service';
import { LoggingService } from '../../shared/modules/utility/service/logging.service';
import { ROUTES } from '../../shared/constants/routes';
import { Router } from '@angular/router';
import { ERROR_MESSAGES } from '../../shared/constants/errors';
import { NotificationService } from '../../shared/components/notification/notification.service';
import { I18N_ERROR_MESSAGES } from '../../shared/i18n/elements';
import { RedirectService } from '../../shared/services/redirect.service';
import { SocialRequest } from '../../shared/api/models/socialRequest';
import { socialProviders } from '../constants/socialProviders';
import { OAuth1SocialRequest } from '../../shared/api/models/oAuth1SocialRequest';
import { OAuthToken } from '../../shared/api/models/oAuthToken';
import { CookieService } from '../../shared/services/cookie.service';
import { COOKIES } from '../../shared/constants/cookieNames';

@Injectable()
export class AuthService {

  constructor(
    private authApi: AuthApi,
    private logger: LoggingService,
    private sessionService: SessionService,
    private injector: Injector,
    private notificationService: NotificationService,
    private redirectService: RedirectService,
    private cookieService: CookieService,
    private zone: NgZone
  ) {}

  public get router(): Router {
    //this creates router property on your service.
    return this.injector.get(Router);
  }

  public authenticateUser(loginCredentials: LoginCredentialsRequest): Observable<User> {
    return this.authApi.login(loginCredentials)
      .map(response => {
        const user = new User(response.payload);
        this.sessionService.authorize(user);
        this.internalRedirect();
        return user;
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  public registerUser(signupCredentials: UserSignupRequest): Observable<User> {
    return this.authApi.signup(signupCredentials)
      .map(response => {
        const user = new User(response.payload);
        this.sessionService.authorize(user);
        this.internalRedirect();
        return user;
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  public restoreSession(): Observable<User> {
    return this.authApi.session()
      .map(response => {
        if (response.payload) {
          const user = new User(response.payload);
          this.sessionService.authorize(user);
          return user;
        } else {
          this.sessionService.dropSession();
        }
        return response;
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  public logout(): Observable<void> {
    return this.authApi.logout()
      .map(response => response.payload)
      .catch(error => Observable.throw(error))
      .finally(() => {
        this.cookieService.set(COOKIES.visitedWelcomePage, true);
        this.sessionService.dropSession();
        this.router.navigate([ROUTES.login])
          .then(() => this.logger.log('navigated to login'))
          .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
      })
      .share();
  }

  public sendSocialRequest(provider: number, input: any): Observable<User> {
    let providerName: string;
    let socialRequest: SocialRequest | OAuth1SocialRequest;

    switch (provider) {
      case socialProviders.FACEBOOK:
        providerName = 'facebook';
        socialRequest = new SocialRequest(input);
        break;
      case socialProviders.GOOGLE:
        providerName = 'google-oauth2';
        socialRequest = new SocialRequest(input);
        break;
      case socialProviders.TWITTER:
        providerName = 'twitter';
        socialRequest = new OAuth1SocialRequest(input);
        break;
      default:
        break;
    }

    return this.authApi.sendSocialRequest(providerName, socialRequest)
      .map(response => {
        const user = new User(response.payload);
        this.sessionService.authorize(user);
        this.internalRedirect();
        return user;
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  public getRequestToken(provider: string): Observable<OAuthToken> {
    return this.authApi.getRequestToken(provider)
      .map(response => {
        return new OAuthToken(response.payload);
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  public getAccessToken(provider: string, data: OAuth1SocialRequest): Observable<OAuthToken> {
    return this.authApi.getAccessToken(provider, data)
      .map(response => {
        return new OAuthToken(response.payload);
      })
      .catch(error => Observable.throw(error))
      .share();
  }

  private internalRedirect(): void {
    const redirectData = this.redirectService.getRedirectData();
    if (redirectData.redirectUrl) {
      this.zone.run(() => {
        this.router.navigate([redirectData.redirectUrl], redirectData.navigationExtras)
          .then(() => {
            this.redirectService.setRedirectUrl(null);
            this.notificationService.show(I18N_ERROR_MESSAGES.createHint, null, 10000);
          })
          .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
      });
    } else {
      this.zone.run(() => {
        this.router.navigate([ROUTES.home])
          .then(() => this.notificationService.show(I18N_ERROR_MESSAGES.createHint, null, 10000))
          .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
      });
    }
  }
}
