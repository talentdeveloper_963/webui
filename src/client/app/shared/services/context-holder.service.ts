import { ElementRef, Injectable } from '@angular/core';

@Injectable()
export class ContextHolderService {
  private modalOverlayElement: ElementRef;

  public setModalOverlayElement(modalOverlayElement: ElementRef) {
    this.modalOverlayElement = modalOverlayElement;
  }

  public getModalOverlayElement(): ElementRef {
    return this.modalOverlayElement;
  }
}
