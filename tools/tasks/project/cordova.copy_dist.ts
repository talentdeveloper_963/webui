import * as gulp from 'gulp';
import Config from '../../config';
import { join } from 'path';

export = () => {
  return gulp.src(join(Config.APP_DEST, '**/*'))
    .pipe(gulp.dest(Config.CORDOVA_WWW));
};
