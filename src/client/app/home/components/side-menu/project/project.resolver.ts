import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProjectService } from './project.service';
import { Project } from '../../../../shared/api/models/project.model';

@Injectable()
export class ProjectResolver implements Resolve<Project> {
  constructor(private projectService: ProjectService) {
  }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> {
    return this.projectService.getProject(route.params.id);
  }
}
