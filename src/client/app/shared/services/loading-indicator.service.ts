import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class LoadingIndicatorService {
  private isBusy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get busy(): BehaviorSubject<boolean> {
    return this.isBusy$;
  }

  public show() {
    this.isBusy$.next(true);
  }

  public hide() {
    this.isBusy$.next(false);
  }
}
