import { Area } from './area';
import { Distance } from '../constants/distance';

export class Camera {
  private _latitude: number;
  private _longitude: number;
  private _zoom: number;


  constructor(latitude: number, longitude: number, zoom: number = Distance.DEFAULT_ZOOM) {
    this._latitude = +latitude;
    this._longitude = +longitude;
    this._zoom = +zoom;
  }

  static toFormatted(coordinate: number | string): number {
    return parseFloat((+coordinate).toFixed(8));
  }

  get latitude(): number {
    return this._latitude;
  }

  set latitude(value: number) {
    this._latitude = value;
  }

  get longitude(): number {
    return this._longitude;
  }

  set longitude(value: number) {
    this._longitude = value;
  }

  get zoom(): number {
    return this._zoom;
  }

  set zoom(value: number) {
    this._zoom = value;
  }

  toArea() {
    return new Area(this.latitude, this.longitude);
  }

  public equals(camera: Camera): boolean {
    return this.latitude === camera.latitude && this.longitude === camera.longitude && this.zoom === camera.zoom;
  }
}

