import { Component, Input } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'cd-bar',
  templateUrl: 'bar.component.html',
  styleUrls: ['bar.component.css'],
})
export class BarComponent {
  @Input() width: number;
  @Input() height: number;
  @Input() value: number;
  @Input() color: string;
}
