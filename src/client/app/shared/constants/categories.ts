export const CATEGORIES = [
  {
    backendName: 'Market',
    iconRainbowUrl: 'assets/graphism/build_your_dream/as_a_mayor/commercial_rainbow.png',
    iconUrl: 'assets/graphism/build_your_dream/as_a_mayor/commercial.png',
  },
  {
    backendName: 'Sport Center',
    iconRainbowUrl: 'assets/graphism/build_your_dream/as_a_mayor/sport_rainbow.png',
    iconUrl: 'assets/graphism/build_your_dream/as_a_mayor/sport.png',
  },
  {
    backendName: 'Cultural Center',
    iconRainbowUrl: 'assets/graphism/build_your_dream/as_a_mayor/cultural_rainbow.png',
    iconUrl: 'assets/graphism/build_your_dream/as_a_mayor/cultural.png',
  },
  {
    backendName: 'University/School',
    iconRainbowUrl: 'assets/graphism/build_your_dream/as_a_mayor/educational_rainbow.png',
    iconUrl: 'assets/graphism/build_your_dream/as_a_mayor/educational.png',
  },
  {
    backendName: 'Hospital',
    iconRainbowUrl: 'assets/graphism/build_your_dream/as_a_mayor/medical_rainbow.png',
    iconUrl: 'assets/graphism/build_your_dream/as_a_mayor/medical.png',
  },
];
