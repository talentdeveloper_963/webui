import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ProjectsApi } from '../../../../shared/api/service/projects';
import { ApiResponseDto } from '../../../../shared/api/common/ApiResponseDto';
import { Project } from '../../../../shared/api/models/project.model';
import { AbstractProjectService } from '../../../../shared/services/abstract-project.service';
import { AbstractSaveProjectRequest } from '../../../../shared/api/common/AbstractSaveProjectRequest';

@Injectable()
export class ProjectService implements AbstractProjectService<Project> {

  constructor(private projectsApi: ProjectsApi) {}

  /**
   * Query for project
   *
   * @param {string} id - unique id of project to load
   * @returns {Observable<Project>} - promise that will resolve project object
   */
  public getProject(id: string): Observable<Project> {
    return this.projectsApi.getProject(id)
      .map((response: ApiResponseDto<Project>) => {
        return new Project(response.payload);
      });
  }

  public getMyProjects(): Observable<Project[]> {
    return this.projectsApi.getMyProjects()
      .map((response: ApiResponseDto<Project[]>) => {
        return response.payload.map((project: Project) => new Project(project));
      });
  }

  public addProject(project: AbstractSaveProjectRequest): Observable<Project> {
    return this.projectsApi.addProject(project)
      .map((response: ApiResponseDto<Project>) => {
        return new Project(response.payload);
      });
  }

  public updateProject(id: string, saveProject: AbstractSaveProjectRequest): Observable<Project> {
    return this.projectsApi.updateProject(id, saveProject)
      .map((response: ApiResponseDto<Project>) => {
        return new Project(response.payload);
      });
  }

  public removeProject(id: string): Observable<any> {
    return this.projectsApi.removeProject(id);
  }

  public submitToVote(id: string): Observable<Project> {
    return this.projectsApi.submitToVote(id)
      .map((response: ApiResponseDto<Project>) => {
        return new Project(response.payload);
      });
  }

  public vote(id: string): Observable<Project> {
    return this.projectsApi.vote(id)
      .map((response: ApiResponseDto<Project>) => {
        return new Project(response.payload);
      });
  }
}
