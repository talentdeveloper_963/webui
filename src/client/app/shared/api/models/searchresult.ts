import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';

//TODO: specify proper type
export class SearchResult extends BaseDto {
  private _coordinates: any;

  /**
   * Initialize a new instance of SearchResult
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.coordinates = input['coordinates'];
  }

  /**
   * Completely clone this instance.
   * @returns SearchResult - Cloned object.
   **/
  public clone(): SearchResult {
    return new SearchResult(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['coordinates'] = this.coordinates;
    return result;
  }

  /**
  * Get value of coordinates
  * @returns - Current value of coordinates.
  **/
  get coordinates(): any {
    return this._coordinates;
  }

  /**
  * Change the value of coordinates.
  * @param newVal - New value to assign.
  **/
  set coordinates(newVal: any) {
    this._coordinates = newVal;
  }


}

