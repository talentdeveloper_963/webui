import { Injectable } from '@angular/core';
import { NavigationService } from '../navigation.service';
import { LocationInfo } from '../../api/models/location-info.models';
import { Location } from '../../api/models/location.models';
import { LoggingService } from '../../modules/utility/service/logging.service';
import { Dictionary } from '../../types';
import { Camera } from '../../models/camera';
import { Point } from '../../api/models/point.model';
import { Distance } from '../../constants/distance';
import { Config } from '../../config/env.config';
import { Router } from '@angular/router';
import { ROUTES } from '../../constants/routes';
import 'mapbox-gl';
import { SessionService } from '../../services/session.service';
import { ViewModeService } from '../../services/view-mode.service';
import { RedirectService } from '../../services/redirect.service';
import { ProjectTypeService } from '../../../home/components/side-menu/project/project-type.service';


@Injectable()
export class MapboxService extends NavigationService {

  private map: mapboxgl.Map = null;

  private markers: Dictionary<mapboxgl.Marker> = {};

  protected clickHandler: (e: mapboxgl.MapMouseEvent) => void;

  constructor(private viewModeService: ViewModeService,
              private redirectService: RedirectService,
              private sessionService: SessionService,
              private projectTypeService: ProjectTypeService,
              private logger: LoggingService,
              private router: Router) {
    super();
    mapboxgl.accessToken = Config.NAVIGATOR_SETTINGS.TOKEN;
  }

  public init(mapContainer: HTMLElement): boolean {
    const startPoint = {
      latitude: 48.864716,
      longitude: 2.349014
    };
    const navigatorSettings = Config.NAVIGATOR_SETTINGS;
    this.map = new mapboxgl.Map({
      container: mapContainer,
      style: navigatorSettings.STYLE,
      minZoom: navigatorSettings.MIN_ZOOM,
      maxZoom: navigatorSettings.MAX_ZOOM,
      zoom: Distance.DEFAULT_ZOOM,
      center: new mapboxgl.LngLat(startPoint.longitude, startPoint.latitude),
      logoPosition: 'bottom-left',
      failIfMayorPerformanceCaveat: true,
      refreshExpiredTiles: false
    });

    if (this.router.url.indexOf(ROUTES.camera) === -1 && this.router.url.indexOf(ROUTES.profile)) {
      this.router.navigate([
        ROUTES.camera,
        Camera.toFormatted(startPoint.latitude),
        Camera.toFormatted(startPoint.longitude),
        Camera.toFormatted(Distance.DEFAULT_ZOOM),
      ]);
    }

    this.map.doubleClickZoom.disable();
    this.map.on('load', () => {
      const layers = this.map.getStyle().layers;
      let labelLayerId;
      for (let i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol') {
          const layout: mapboxgl.SymbolLayout = layers[i].layout as mapboxgl.SymbolLayout;
          if (layout['text-field']) {
            labelLayerId = layers[i].id;
          }
          break;
        }
      }
      this.map.addLayer({
        'id': '3d-buildings',
        'source': 'composite',
        'source-layer': 'building',
        'filter': ['==', 'extrude', 'true'],
        'type': 'fill-extrusion',
        'minzoom': 15,
        'paint': {
          'fill-extrusion-color': '#ffffff',
          'fill-extrusion-height': [
            'interpolate', ['linear'], ['zoom'],
            15, 0,
            15.05, ['get', 'height']
          ],
          'fill-extrusion-base': [
            'interpolate', ['linear'], ['zoom'],
            15, 0,
            15.05, ['get', 'min_height']
          ],
          'fill-extrusion-opacity': .8
        }
      }, labelLayerId);

    });
    this.map.on('moveend', () => this.handleViewportChange(this.map.getCenter()));

    this.map.on('pitchend', () => this.updateMapPitchValueSource());

    this.updateMapPitchValueSource();

    return true;
  }

  public updateMapPitchValueSource() {
    this.mapPitchValueSource.next(this.map.getPitch());
  }

  public setClickHandler(customClickHandler?: (e: mapboxgl.MapMouseEvent) => void) {
    this.clickHandler = customClickHandler || this.defaultClickHandler.bind(this);
    this.map.on('click', this.clickHandler);
  }

  public removeClickHandler() {
    this.map.off('click', this.clickHandler);
  }

  public addLocationMarkers(locations: Array<LocationInfo>) {
    for (const location of locations) {
      this.renderMarker(location.id, location.coordinates, location.type);
    }
  }

  public addLocationMarker(location: Location, type?: string) {
    //TODO: change input argument type to LocationInfo
    this.renderMarker(location.id, location.coordinates, type ? type : location.type);
  }

  private renderMarker(id: string, point: Point, type: string) {
    const container = document.createElement('div');
    container.classList.add('marker-container');

    const icon = document.createElement('img');
    icon.id = id;
    if (!type) {
      container.classList.add('location-icon-container');
      icon.classList.add('location-marker');
      icon.setAttribute('src', 'assets/pin.png');
    } else {
      this.projectTypeService.getProjectTypeByName(type).subscribe((typeSettings) => {
        container.classList.add('project-icon-container');
        container.style.backgroundColor = typeSettings.color;

        icon.classList.add('project-icon');
        icon.setAttribute('src', typeSettings.url);
      });
    }
    container.addEventListener('click', (e: any) => {
      e.stopPropagation();
      if (this.canSelectLocation) {
        this.setLocation(e.target.id || e.target.firstChild.id);
      }
    });
    container.addEventListener('drag', (e: DragEvent) => {
      e.stopPropagation();
    });
    container.appendChild(icon);
    if (!(id in this.markers)) {
      this.markers[id] = new mapboxgl.Marker(container).setLngLat([point.longitude, point.latitude]).addTo(this.map);
    } else {
      this.removeLocationMarker(id);
      this.markers[id] = new mapboxgl.Marker(container).setLngLat([point.longitude, point.latitude]).addTo(this.map);
    }
  }

  public removeLocationMarker(id: string) {
    if (id in this.markers) {
      const marker = this.markers[id];
      marker.remove();
      delete this.markers[id];
    }
  }

  public removeAllLocationMarkers() {
    for (const key in this.markers) {
      if (this.markers.hasOwnProperty(key)) {
        const marker = this.markers[key];
        marker.remove();
      }
    }
    this.markers = {};
  }

  public pitchMap(pitch: number) {
    if (pitch !== this.map.getPitch()) {
      this.map.setPitch(pitch);
    }
  }

  public zoomIn(): boolean {
    if (this.map.getZoom() <= this.map.getMaxZoom()) {
      this.map.zoomIn();
      return true;
    } else {
      return false;
    }
  }

  public zoomOut(): boolean {
    if (this.map.getZoom() >= this.map.getMinZoom()) {
      this.map.zoomOut({});
      return true;
    } else {
      return false;
    }
  }

  public flyToByCoordinates(camera: Camera, onComplete?: () => void) {
    this.map.flyTo({
      center: new mapboxgl.LngLat(camera.longitude, camera.latitude),
      zoom: camera.zoom
    });
  }

  public freezeCamera() {
    this.map.scrollZoom.disable();
    this.map.dragRotate.disable();
    this.map.boxZoom.disable();
    this.map.dragPan.disable();
    this.map.keyboard.disable();
    this.map.touchZoomRotate.disable();
  }

  public unfreezeCamera() {
    this.map.scrollZoom.enable();
    this.map.dragRotate.enable();
    this.map.boxZoom.enable();
    this.map.dragPan.enable();
    this.map.keyboard.enable();
    this.map.touchZoomRotate.enable();
  }

  public getCameraPosition(): Camera {
    const center = this.map.getCenter();
    const zoom = this.map.getZoom();
    return new Camera(center.lat, center.lng, zoom);
  }

  enableCameraRotation(enable: boolean): boolean {
    return false;
  }

  enableCameraZoom(enable: boolean): boolean {
    return false;
  }

  flyTo(params: object): void {
  }

  getCurrentCameraCoordinates(): object {
    return undefined;
  }

  orientationByPositionAndRotation(position: object, rotation: number): boolean {
    return false;
  }

  pickObject(position: object): boolean {
    return false;
  }

  windowPositionToCartesian(position: object): boolean {
    return false;
  }

  windowPositionToCartographic(position: object): boolean {
    return false;
  }

  public setLocation(locationId?: string) {
    this.locationSelectedSource.next(locationId);
  }

  public lockClick() {
    this.clickLocked = true;
  }

  public unlockClick() {
    this.clickLocked = false;
  }

  public allowLocationSelection() {
    this.canSelectLocation = true;
  }

  public forbidLocationSelection() {
    this.canSelectLocation = false;
  }

  protected defaultClickHandler(e: mapboxgl.MapMouseEvent) {
    if (!this.clickLocked) {
      this.lockClick();
      this.handleLocationCreation(e.lngLat);
    }
  }

  private handleViewportChange(center: mapboxgl.LngLat) {
    const zoom = this.map.getZoom();
    const camera = new Camera(Camera.toFormatted(center.lat), Camera.toFormatted(center.lng), Camera.toFormatted(zoom));
    this.cameraChangedSource.next(camera);
  }

  private handleLocationCreation(coordinates: mapboxgl.LngLat) {
    const zoom = Camera.toFormatted(this.map.getZoom());
    if (this.sessionService.isAuthorized() && zoom >= Distance.DEFAULT_ZOOM) {
      const point = new Point({latitude: coordinates.lat, longitude: coordinates.lng});
      this.locationCreatedSource.next(point);
    } else if (!this.sessionService.isAuthorized()) {
      this.unlockClick();
      return this.viewModeService.showViewModeDialog()
        .subscribe(() => {
          this.redirectService.setRedirectUrl(null);
          return false;
        });
    } else {
      this.unlockClick();
    }
  }
}
