import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { I18N_HEADERS } from '../../i18n/elements';
import * as _ from 'lodash';

@Component({
  moduleId: module.id,
  selector: 'cd-nav-hints-dialog',
  templateUrl: 'nav-hints-dialog.component.html',
  styleUrls: ['nav-hints-dialog.component.css']
})
export class NavHintsDialogComponent {
  @Input() width: number;
  @Input() showDialog: boolean;
  @Input() positionLeft: number;
  @Input() positionTop: number;
  @Output() hide: EventEmitter<any> = new EventEmitter();
  I18N_HEADERS = I18N_HEADERS;
  labels: any;

  constructor(private translate: TranslateService) {
    this.getLabels();
    this.translate.onLangChange
      .subscribe(() => {
        this.getLabels();
      });
  }

  getLabels() {
    this.translate.get(_.values(I18N_HEADERS))
      .subscribe(res => this.labels = res);
  }

}
