import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class SaveLocationRequest extends BaseDto {
  private _address: string;
  private _coordinates: Point;

  /**
   * Initialize a new instance of SaveLocationRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.address = input['address'];
      this.coordinates = input['coordinates'] ? new Point(input['coordinates']) : null;
  }

  /**
   * Completely clone this instance.
   * @returns SaveLocationRequest - Cloned object.
   **/
  public clone(): SaveLocationRequest {
    return new SaveLocationRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['address'] = this.address;
    result['coordinates'] = this.coordinates ? this.coordinates.toJSON() : null;
    return result;
  }

  /**
  * Get value of address
  * @returns - Current value of address.
  **/
  get address(): string {
    return this._address;
  }

  /**
  * Change the value of address.
  * @param newVal - New value to assign.
  **/
  set address(newVal: string) {
    this._address = newVal;
  }

  /**
  * Get value of coordinates
  * @returns - Current value of coordinates.
  **/
  get coordinates(): Point {
    return this._coordinates;
  }

  /**
  * Change the value of coordinates.
  * @param newVal - New value to assign.
  **/
  set coordinates(newVal: Point) {
    this._coordinates = newVal;
  }


}

import { Point } from './point.model';
