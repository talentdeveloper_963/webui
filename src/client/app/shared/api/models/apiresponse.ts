import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ApiResponse extends BaseDto {
  private _service: ServiceSection;
  private _payload: any;

  /**
   * Initialize a new instance of ApiResponse
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.service = input['service'] ? new ServiceSection(input['service']) : null;
      this.payload = input['payload'];
  }

  /**
   * Completely clone this instance.
   * @returns ApiResponse - Cloned object.
   **/
  public clone(): ApiResponse {
    return new ApiResponse(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['service'] = this.service ? this.service.toJSON() : null;
    result['payload'] = this.payload;
    return result;
  }

  /**
  * Get value of service
  * @returns - Current value of service.
  **/
  get service(): ServiceSection {
    return this._service;
  }

  /**
  * Change the value of service.
  * @param newVal - New value to assign.
  **/
  set service(newVal: ServiceSection) {
    this._service = newVal;
  }

  /**
  * Response payload
  * @returns - Current value of payload.
  **/
  get payload(): any {
    return this._payload;
  }

  /**
  * Change the value of payload.
  * @param newVal - New value to assign.
  **/
  set payload(newVal: any) {
    this._payload = newVal;
  }


}

import { ServiceSection } from './servicesection';
