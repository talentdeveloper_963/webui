import { Component, Input } from '@angular/core';
import * as _ from 'lodash';

@Component({
  moduleId: module.id,
  selector: 'cd-star-rating',
  templateUrl: 'star-rating.component.html',
  styleUrls: ['star-rating.component.css'],
})
export class StarRatingComponent {
  @Input() rating: number;
  private _filledStars: number[];
  private _notFilledStars: number[];

  get filledStars() {
    return _.range(this.rating);
  }

  set filledStars(value: number[]) {
    this._filledStars = value;
  }

  get notFilledStars() {
    return _.range(5 - this.rating);
  }

  set notFilledStars(value: number[]) {
    this._notFilledStars = value;
  }
}
