import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class FindNearestInspirationalMessageRequest extends BaseDto {
  private _camera: Point;
  private _messages_read: Array<string>;
  private _type: string;

  /**
   * Initialize a new instance of FindNearestInspirationalMessageRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.camera = input['camera'] ? new Point(input['camera']) : null;
      this.messagesRead = input['messages_read'];
      this.type = input['type'];
  }

  /**
   * Completely clone this instance.
   * @returns FindNearestInspirationalMessageRequest - Cloned object.
   **/
  public clone(): FindNearestInspirationalMessageRequest {
    return new FindNearestInspirationalMessageRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['camera'] = this.camera ? this.camera.toJSON() : null;
    result['messages_read'] = this.messagesRead;
    result['type'] = this.type;
    return result;
  }

  /**
  * Get value of camera
  * @returns - Current value of camera.
  **/
  get camera(): Point {
    return this._camera;
  }

  /**
  * Change the value of camera.
  * @param newVal - New value to assign.
  **/
  set camera(newVal: Point) {
    this._camera = newVal;
  }

  /**
  * Get value of messages_read
  * @returns - Current value of messages_read.
  **/
  get messagesRead(): Array<string> {
    return this._messages_read;
  }

  /**
  * Change the value of messages_read.
  * @param newVal - New value to assign.
  **/
  set messagesRead(newVal: Array<string>) {
    this._messages_read = newVal;
  }

  /**
  * Get value of type
  * @returns - Current value of type.
  **/
  get type(): string {
    return this._type;
  }

  /**
  * Change the value of type.
  * @param newVal - New value to assign.
  **/
  set type(newVal: string) {
    this._type = newVal;
  }


}

import { Point } from './point.model';
