import * as moment from 'moment';
import 'moment/locale/ru';
import 'moment/locale/fr';
import 'moment/locale/de';
import 'moment/locale/es';
import 'moment/locale/it';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LanguageService {
  private languageChangedSource: BehaviorSubject<string> = new BehaviorSubject<string>('en');
  public languageChanged: Observable<string> = this.languageChangedSource.asObservable();

  constructor(
    private translate: TranslateService,
  ) {}

  public setLanguage(languageCode: string) {
    this.languageChangedSource.next(languageCode);
    moment.locale(languageCode);
    this.translate.use(languageCode);
  }

  public getSelectedLanguage(): string {
    return this.languageChangedSource.value;
  }
}
