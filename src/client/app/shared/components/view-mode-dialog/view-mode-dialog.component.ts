import { Component, Input, OnDestroy } from '@angular/core';
import { ViewModeService } from '../../services/view-mode.service';
import { Router } from '@angular/router';
import { ROUTES } from '../../constants/routes';

@Component({
  moduleId: module.id,
  selector: 'cd-view-mode-dialog',
  templateUrl: 'view-mode-dialog.component.html',
  styleUrls: ['view-mode-dialog.component.css'],
})
export class ViewModeDialogComponent implements OnDestroy {
  @Input() showDialog: boolean;

  constructor(
    private router: Router,
    private viewModeService: ViewModeService
  ) {}

  ngOnDestroy() {
    this.viewModeService.hideViewModeDialog();
  }

  signIn() {
    this.viewModeService.hideViewModeDialog();
    this.router.navigate([ROUTES.login]);
  }

  signUp() {
    this.viewModeService.hideViewModeDialog();
    this.router.navigate([ROUTES.register]);
  }

  onCancel() {
    this.viewModeService.hideViewModeDialog();
    this.viewModeService.cancel.emit();
  }
}
