import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { CalendarLocale, DEFAULT_CALENDAR_LOCALE } from '../../i18n/calendar';
import { CodemosTranslateStore } from '../../i18n/translate-store';

@Component({
  moduleId: module.id,
  selector: 'cd-date-picker',
  templateUrl: 'date-picker.component.html',
  styleUrls: ['date-picker.component.css'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatePickerComponent), multi: true}
  ]
})
export class DatePickerComponent implements ControlValueAccessor, OnInit {
  @Input() _dateValue: Date;
  @Input() showTime: boolean;
  @Input() styleClass: string;
  @Input() yearNavigator: boolean;
  @Input() yearRange: string;
  @Input() inputId: string;
  @Input() inputStyleClass: string;
  @Input() placeholder: string;

  @Output() bySelect: EventEmitter<void> = new EventEmitter<void>();
  @Output() byBlur: EventEmitter<void> = new EventEmitter<void>();

  public locale: CalendarLocale = DEFAULT_CALENDAR_LOCALE;

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
    this.getCalendarLocale();
    this.translate.onLangChange
      .subscribe(() => {
        this.getCalendarLocale();
      });
  }

  /* tslint:disable:no-empty */
  propagateChange: any = () => {};

  get dateValue() {
    return this._dateValue;
  }

  set dateValue(val: Date) {
    this._dateValue = val;
    this.propagateChange(val.toISOString());
  }

  writeValue(value: string): void {
    if (value) {
      this.dateValue = new Date(value);
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  public onSelect(): void {
    this.bySelect.emit();
  }

  public onBlur(): void {
    this.byBlur.emit();
  }

  private getCalendarLocale(): void {
    const lang = this.translate.currentLang;
    const localeSubscription = this.translate.getTranslation(lang)
      .subscribe((trans: CodemosTranslateStore) => {
        this.locale = trans._calendar;
        localeSubscription.unsubscribe();
      });
  }
}
