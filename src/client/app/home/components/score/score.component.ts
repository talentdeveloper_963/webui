import { Component, Input } from '@angular/core';
import { SessionService } from '../../../shared/services/session.service';
import { UserPrincipal } from '../../../shared/models/SecurityPrincipal';

@Component({
  moduleId: module.id,
  selector: 'cd-score',
  templateUrl: 'score.component.html',
  styleUrls: ['score.component.css']
})
export class ScoreComponent {
  @Input() score: number;

  constructor(private sessionService: SessionService) {
  }

  getCurrentScore(): number {
    if (!this.score && <UserPrincipal>this.sessionService.getSecurityPrincipal()) {
      return (<UserPrincipal>this.sessionService.getSecurityPrincipal()).user.score;
    } else if (this.score) {
      return this.score;
    } else {
      return null;
    }
  }
}
