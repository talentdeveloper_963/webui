import * as gulp from 'gulp';
import * as htmlmin from 'gulp-htmlmin';
import { join } from 'path';
import Config from '../../config';

export = () => {
  return gulp.src([join(Config.APP_DEST, 'oauth/redirect.html')])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      minifyJS: true
    }))
    .pipe(gulp.dest(join(Config.APP_DEST, 'oauth')));
};
