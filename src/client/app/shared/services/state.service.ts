import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class StateService {
  public locationModalStateChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public selectLocation$: EventEmitter<string> = new EventEmitter<string>();
  public closeLocation$: EventEmitter<void> = new EventEmitter<void>();
  private overlayDialogShow: boolean;

  public showOverlayDialog() {
    this.overlayDialogShow = true;
  }

  public hideOverlayDialog() {
    this.overlayDialogShow = false;
  }

  public isOverlayDialogShown() {
    return this.overlayDialogShow;
  }

  public showLocationModal() {
    this.locationModalStateChange.emit(true);
  }

  public hideLocationModal() {
    this.locationModalStateChange.emit(false);
  }

  public selectLocation(type: string) {
    this.selectLocation$.emit(type);
  }

  public closeLocation() {
    this.closeLocation$.emit();
  }
}
