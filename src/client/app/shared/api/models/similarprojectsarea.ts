import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class SimilarProjectsArea extends BaseDto {
  private _coordinates: Point;
  private _radius: number;
  private _category_id: string;
  private _resource_id: string;

  /**
   * Initialize a new instance of SimilarProjectsArea
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.coordinates = input['coordinates'] ? new Point(input['coordinates']) : null;
      this.radius = input['radius'];
      this.categoryId = input['category_id'];
      this.resourceId = input['resource_id'];
  }

  /**
   * Completely clone this instance.
   * @returns SimilarProjectsArea - Cloned object.
   **/
  public clone(): SimilarProjectsArea {
    return new SimilarProjectsArea(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['coordinates'] = this.coordinates ? this.coordinates.toJSON() : null;
    result['radius'] = this.radius;
    result['category_id'] = this.categoryId;
    result['resource_id'] = this.resourceId;
    return result;
  }

  /**
  * Get value of coordinates
  * @returns - Current value of coordinates.
  **/
  get coordinates(): Point {
    return this._coordinates;
  }

  /**
  * Change the value of coordinates.
  * @param newVal - New value to assign.
  **/
  set coordinates(newVal: Point) {
    this._coordinates = newVal;
  }

  /**
  * Get value of radius
  * @returns - Current value of radius.
  **/
  get radius(): number {
    return this._radius;
  }

  /**
  * Change the value of radius.
  * @param newVal - New value to assign.
  **/
  set radius(newVal: number) {
    this._radius = newVal;
  }

  /**
  * Get value of category_id
  * @returns - Current value of category_id.
  **/
  get categoryId(): string {
    return this._category_id;
  }

  /**
  * Change the value of category_id.
  * @param newVal - New value to assign.
  **/
  set categoryId(newVal: string) {
    this._category_id = newVal;
  }

  /**
  * Get value of resource_id
  * @returns - Current value of resource_id.
  **/
  get resourceId(): string {
    return this._resource_id;
  }

  /**
  * Change the value of resource_id.
  * @param newVal - New value to assign.
  **/
  set resourceId(newVal: string) {
    this._resource_id = newVal;
  }


}

import { Point } from './point.model';
