import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';
import { ProjectType } from '../models/project-type.model';

@Injectable()
export class ProjectTypeApi extends BaseApi {

  public getAvailableProjectTypes(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<ProjectType>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('get')
      .withPath(`/types`);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }
}
