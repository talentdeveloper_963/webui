import { Component, OnDestroy } from '@angular/core';
import { AbstractProjectViewComponent } from '../../../../../../shared/components/project-view/project-view.component';
import { LocationService } from '../../../location/location.service';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { ProjectService } from '../../project.service';
import { Project } from '../../../../../../shared/api/models/project.model';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { I18N_ERROR_MESSAGES } from '../../../../../../shared/i18n/elements';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { SessionService } from '../../../../../../shared/services/session.service';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { ProjectCategoryService } from '../../project-category.service';
import { ProjectCategory } from '../../../../../../shared/api/models/project-category.model';
import { ProjectTypeService } from '../../project-type.service';
import { ProjectType } from '../../../../../../shared/api/models/project-type.model';

@Component({
  moduleId: module.id,
  selector: 'cd-project-view',
  templateUrl: 'project-view.activity.html'
})
export class ProjectViewActivityComponent extends AbstractProjectViewComponent<Project> implements OnDestroy {
  public categories: ProjectCategory[];

  public projectType: ProjectType;

  public subcategoryError: string;

  public LABELS = CONSTANTS.PROJECT.VIEW;

  constructor(
    protected projectService: ProjectService,
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected projectTypeService: ProjectTypeService,
    protected locationService: LocationService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected stateService: StateService,
    protected promoteThemService: PromoteThemService,
    protected navigationService: NavigationService,
    protected sessionService: SessionService,
    private categoryService: ProjectCategoryService,
  ) {
    super(
      projectService,
      locationService,
      loadingIndicatorService,
      router,
      route,
      promoteThemService,
      stateService,
      navigationService,
      sessionService,
      translate,
      notificationService,
      projectTypeService
    );
  }

  protected afterSetProject() {
    this.projectTypeService.getProjectTypeByName(this.project.category.type)
      .subscribe((projectType: ProjectType) => {
        this.projectType = projectType;
        this.categoryService.getCategories()
          .subscribe((categories: ProjectCategory[]) => {
            this.categories = categories.filter((category: ProjectCategory) => category.type === this.projectType.name);
          });
      });
  }

  public removeProject() {
    super.removeProject('project');
  }

  public nextProject() {
    super.nextProject(PROJECT_TYPES.PUBLIC_PROJECT);
  }

  public isVisible() {
    return super.isVisible();
  }

  public onHide() {
    super.onHide();
  }

  public onShow() {
    super.onShow();
  }

  protected onSubmitError() {
    this.subcategoryError = 'Another similar project is already built in this area.';
    this.translate.get(I18N_ERROR_MESSAGES.subcategoryError).subscribe(res => {
      this.notificationService.show(res);
    });
  }
}
