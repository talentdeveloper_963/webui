import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class SearchApi extends BaseApi {


  /**
   * Search by address
   * @remarks Operation handler for searchByAddress
   * @param { string } query  
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public searchByAddress(query: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<SearchResult> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/search/address`);
    if (query) {
      queryParams['query'] = String(query);
    }
    
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}

import { SearchResult } from '../models/searchresult';
