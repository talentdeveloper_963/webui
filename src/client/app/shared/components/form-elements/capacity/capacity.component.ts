import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { VALIDATIONS } from '../../../../auth/constants/validations';
import { Dictionary } from '../../../types';

@Component({
  moduleId: module.id,
  selector: 'cd-capacity-field',
  templateUrl: 'capacity.component.html',
  styleUrls: ['capacity.component.css'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CapacityComponent), multi: true}
  ]
})

export class CapacityComponent implements ControlValueAccessor {
  @Input() control: FormControl;
  @Input() constraint: Dictionary<any>;

  public VALIDATIONS: any = VALIDATIONS;

  private _value: string;

  constructor() {
  }

  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this.propagateChange(parseFloat(value));
  }

  writeValue(value: number): void {
    if (value) {
      this._value = value.toString();
    }
  }

  propagateChange: any = () => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

}
