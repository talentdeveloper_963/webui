import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { ROUTES } from '../constants/routes';
import { AuthService } from '../../auth/services/auth.service';
import { LoggingService } from '../modules/utility/service/logging.service';
import { ERROR_MESSAGES } from '../constants/errors';
import { Observable } from 'rxjs/Observable';
import { ViewModeService } from '../services/view-mode.service';
import { RedirectService } from '../services/redirect.service';
import { LocationService } from '../../home/components/side-menu/location/location.service';
import { NavigationService } from '../map/navigation.service';

@Injectable()
export class AuthorizedGuard implements CanActivate {
  private urlSegments: string[];

  constructor(private sessionService: SessionService,
              private router: Router,
              private authService: AuthService,
              private logger: LoggingService,
              private navigationService: NavigationService,
              private locationService: LocationService,
              private viewModeService: ViewModeService,
              private redirectService: RedirectService) {
  }

  getUrlSegments(root: any) {
    root.segments.forEach((segment: UrlSegment) => {
      this.urlSegments.push(segment.path);
    });
    return this.urlSegments;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (!this.sessionService.isAuthorized()) {
      this.urlSegments = [];
      const parsedUrl = this.router.parseUrl(state.url);
      const urlSegments = this.getUrlSegments(parsedUrl.root.children.primary);
      this.redirectService.setRedirectUrl(urlSegments && urlSegments.length > 1 ? '/' + urlSegments.join('/') : null, {queryParams: parsedUrl.queryParams});
      return this.viewModeService.showViewModeDialog()
        .subscribe(() => {
          this.redirectService.setRedirectUrl(null);
          return false;
        });
    } else {
      return this.authService.restoreSession()
        .map(() => {
          if (!this.sessionService.isAuthorized()) {
            this.router.navigate([ROUTES.login])
              .then(() => this.logger.log('navigated to login'))
              .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
            return false;
          }
          return true;
        });
    }
  }
}
