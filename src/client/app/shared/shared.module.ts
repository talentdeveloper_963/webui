import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { BaseRequestBehavior } from './api/common/BaseRequestBehavior';
import { UtilityModule } from './modules/utility/utility.module';
import { LoadingIndicatorService } from './services/loading-indicator.service';
import { NotificationService } from './components/notification/notification.service';
import { LogoComponent } from './components/logo/logo.component';
import { LanguageSwitcherComponent } from './components/language-switcher/language-switcher.component';
import { CalendarModule, ChartModule, DialogModule, DropdownModule, TabViewModule } from 'primeng/primeng';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { AsteriskComponent } from './components/asterisk/asterisk.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
import { NavHintsDialogComponent } from './components/nav-hints-dialog/nav-hints-dialog.component';
import { ValidationMessagesComponent } from './components/validation-messages/validation-messages.component';
import { ProjectButtonsComponent } from './components/project-buttons/project-buttons.component';
import { ProjectImpactComponent } from './components/project-impact/project-impact.component';
import { BarComponent } from './components/bar/bar.component';
import { SizeService } from './services/size.service';
import { CordovaService } from './services/cordova.service';
import { CapacityComponent } from './components/form-elements/capacity/capacity.component';
import { OnlyNumbersDirective } from './directives/onlyNumbers.directive';
import { ConditionService } from './services/condition.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { RedirectService } from './services/redirect.service';
import { ViewModeDialogComponent } from './components/view-mode-dialog/view-mode-dialog.component';
import { ViewModeService } from './services/view-mode.service';
import { TrimDirective } from './directives/trim.directive';
import { StateService } from './services/state.service';
import { KeysPipe } from './pipes/keys.pipe';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { ApiMockService } from '../testing/services/api-mock.service';
import { CookieService } from './services/cookie.service';
import { MathService } from './services/math.service';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UtilityModule,
    DropdownModule,
    DialogModule,
    TabViewModule,
    TranslateModule.forChild(),
    CalendarModule,
    ChartModule,
    NgSelectModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CalendarModule,
    LogoComponent,
    LanguageSwitcherComponent,
    StarRatingComponent,
    AsteriskComponent,
    ConfirmDialogComponent,
    NavHintsDialogComponent,
    ValidationMessagesComponent,
    ProjectButtonsComponent,
    ChartModule,
    ProjectImpactComponent,
    BarComponent,
    CapacityComponent,
    OnlyNumbersDirective,
    NgSelectModule,
    ViewModeDialogComponent,
    TrimDirective,
    KeysPipe,
    DatePickerComponent
  ],
  declarations: [
    LogoComponent,
    LanguageSwitcherComponent,
    StarRatingComponent,
    AsteriskComponent,
    ConfirmDialogComponent,
    NavHintsDialogComponent,
    ValidationMessagesComponent,
    ProjectButtonsComponent,
    ProjectImpactComponent,
    BarComponent,
    CapacityComponent,
    OnlyNumbersDirective,
    ViewModeDialogComponent,
    TrimDirective,
    KeysPipe,
    DatePickerComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        BaseRequestBehavior,
        LoadingIndicatorService,
        NotificationService,
        SizeService,
        CordovaService,
        ConditionService,
        RedirectService,
        ViewModeService,
        StateService,
        CookieService,
        ApiMockService,
        MathService
      ]
    };
  }
}
