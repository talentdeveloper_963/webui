import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class LoginCredentialsRequest extends BaseDto {
  private _email: string;
  private _password: string;
  private _remember_me: boolean;

  /**
   * Initialize a new instance of LoginCredentialsRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.email = input['email'];
      this.password = input['password'];
      this.rememberMe = input['remember_me'];
  }

  /**
   * Completely clone this instance.
   * @returns LoginCredentialsRequest - Cloned object.
   **/
  public clone(): LoginCredentialsRequest {
    return new LoginCredentialsRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['email'] = this.email;
    result['password'] = this.password;
    result['remember_me'] = this.rememberMe;
    return result;
  }

  /**
  * Get value of email
  * @returns - Current value of email.
  **/
  get email(): string {
    return this._email;
  }

  /**
  * Change the value of email.
  * @param newVal - New value to assign.
  **/
  set email(newVal: string) {
    this._email = newVal.trim().toLowerCase();
  }

  /**
  * Get value of password
  * @returns - Current value of password.
  **/
  get password(): string {
    return this._password;
  }

  /**
  * Change the value of password.
  * @param newVal - New value to assign.
  **/
  set password(newVal: string) {
    this._password = newVal;
  }

  /**
  * Get value of remember_me
  * @returns - Current value of remember_me.
  **/
  get rememberMe(): boolean {
    return this._remember_me;
  }

  /**
  * Change the value of remember_me.
  * @param newVal - New value to assign.
  **/
  set rememberMe(newVal: boolean) {
    this._remember_me = newVal;
  }


}

