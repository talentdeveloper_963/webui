import { AbstractProject } from '../../../../../shared/api/common/AbstractProject';
import { PROJECT_TYPES } from '../../../../../shared/constants/projectTypes';
import { Injectable } from '@angular/core';

@Injectable()
export class PromoteThemService {
  private display = false;
  private project: AbstractProject;
  private projectType: string;

  show(project: AbstractProject, type: string) {
    this.project = project;
    this.projectType = type;
    this.display = true;
  }

  hide() {
    this.display = false;
  }

  isVisible(): boolean {
    return this.display;
  }

  getProject(): AbstractProject {
    return this.project;
  }

  getProjectType(): string {
    return this.projectType;
  }

  getProjectTypeString(): string {
    switch (this.projectType) {
      case PROJECT_TYPES.PUBLIC_PROJECT:
        return 'project';
      case PROJECT_TYPES.EVENT:
        return 'event';
      case PROJECT_TYPES.IDEA:
        return 'inspire';
      default:
        return '';
    }
  }
}
