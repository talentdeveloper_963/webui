import {
  AfterViewChecked, AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { LoadingIndicatorService } from '../../shared/services/loading-indicator.service';
import { LocationService } from '../components/side-menu/location/location.service';
import { Event, NavigationStart, Router } from '@angular/router';
import { PromoteThemService } from '../components/side-menu/promote/promote-them/promote-them.service';
import { SecurityPrincipal } from '../../shared/models/SecurityPrincipal';
import { SessionService } from '../../shared/services/session.service';
import { CONSTANTS } from '../../shared/constants/constants';
import { ROUTES } from '../../shared/constants/routes';
import { NavigationService } from '../../shared/map/navigation.service';
import { Subscription } from 'rxjs/Subscription';
import { ContextHolderService } from '../../shared/services/context-holder.service';
import { StateService } from '../../shared/services/state.service';
import { CookieService } from '../../shared/services/cookie.service';
import { COOKIES } from '../../shared/constants/cookieNames';
import { RedirectService } from '../../shared/services/redirect.service';
import { MathService } from '../../shared/services/math.service';
import { Distance } from '../../shared/constants/distance';
import { Dictionary } from '../../shared/types';


@Component({
  moduleId: module.id,
  selector: 'codemos-home',
  templateUrl: 'home.activity.html',
  styleUrls: ['home.activity.css'],
})
export class HomeActivityComponent implements OnInit, OnDestroy, AfterViewInit {
  public isBusy = false;
  public securityPrincipal: SecurityPrincipal = null;
  public hintsDialogState = false;
  public showInfoDropdown: boolean;
  public pitchValue: number;
  public distance: Dictionary<any> = Distance;

  @ViewChild('overlay') overlay: ElementRef;
  @HostListener('document:click', ['$event']) clickOutside(event: any) {
    this.showInfoDropdown = false;
  }

  private _loadingSubscription: Subscription;

  constructor(
    private loadingIndicatorService: LoadingIndicatorService,
    private sessionService: SessionService,
    private locationService: LocationService,
    private router: Router,
    private stateService: StateService,
    private promoteService: PromoteThemService,
    private navigationService: NavigationService,
    private cdRef: ChangeDetectorRef,
    private cookieService: CookieService,
    private contextHolderService: ContextHolderService,
    private redirectService: RedirectService,
    private mathService: MathService,
  ) {}

  public ngOnInit() {
    this.navigationService.mapPitchValue$.subscribe(pitch => this.pitchValue = pitch);

    this._loadingSubscription = this.loadingIndicatorService.busy.subscribe((isBusy) => {
      this.isBusy = isBusy;
      this.cdRef.detectChanges();
    });

    if (this.sessionService.isAuthorized()) {
      this.securityPrincipal = this.sessionService.getSecurityPrincipal();
    }

    this.sessionService.getSecurityPrincipalSubject()
      .subscribe((securityPrincipal: SecurityPrincipal) => {
        this.securityPrincipal = securityPrincipal;
      });

    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.promoteService.hide();
      }
    });
  }

  public ngOnDestroy() {
    this._loadingSubscription.unsubscribe();
  }

  public ngAfterViewInit() {
    this.contextHolderService.setModalOverlayElement(this.overlay);
  }

  public showOverlay() {
    return this.stateService.isOverlayDialogShown();
  }

  public pitchIncrease() {
    let degree = this.mathService.ceil10(this.pitchValue);
    if (degree > Distance.MAX_PITCH) {
      degree = Distance.MAX_PITCH;
    }
    this.navigationService.pitchMap(degree);
  }

  public pitchDecrease() {
    let degree = this.mathService.floor10(this.pitchValue);
    if (degree < Distance.MIN_PITCH) {
      degree = Distance.MAX_PITCH;
    }
    this.navigationService.pitchMap(degree);
  }

  public zoomIn() {
    this.navigationService.zoomIn();
  }

  public zoomOut() {
    this.navigationService.zoomOut();
  }

  public closeHints() {
    this.hintsDialogState = false;
  }

  public toggleInfoDropdown(event: any) {
    this.showInfoDropdown = !this.showInfoDropdown;
  }

  public goToAboutTheGame() {
    this.router.navigate([ROUTES.about]);
  }

  public goToGameRules() {
    this.router.navigate([ROUTES.rules]);
  }

  public goToNavHints() {
    this.hintsDialogState = true;
  }

  public goToAttributionsPage() {
    this.router.navigate([ROUTES.attributions]);
  }

  public infoWrapperClick(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }
}
