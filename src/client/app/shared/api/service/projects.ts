import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class ProjectsApi extends BaseApi {


  /**
   * Get user&#x27;s projects
   * @remarks Operation handler for getMyProjects

   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getMyProjects(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<Project>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/project/my`);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get project
   * @remarks Operation handler for getProject
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getProject(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/project/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Update user&#x27;s project
   * @remarks Operation handler for updateProject
   * @param { string } id  * @param { SaveProjectRequest } saveProject
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public updateProject(id: string, saveProject: AbstractSaveProjectRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('put')
                  .withPath(`/project/${id}`);

    pathParams['id'] = String(id);


    if (saveProject) {
      rqBuilder.withJsonBody(saveProject);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Remove Project
   * @remarks Operation handler for removeProject
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public removeProject(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('delete')
                  .withPath(`/project/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Submit to vote
   * @remarks Operation handler for submitToVote
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public submitToVote(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/project/${id}/submit`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Vote
   * @remarks Operation handler for vote
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public vote(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/project/${id}/vote`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Share project by id
   * @remarks Share project by id
   * @param { string } id  * @param { ShareProviderRequest } share
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public shareProject(id: string, share: ShareProviderRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/project/${id}/share`);

    pathParams['id'] = String(id);


    if (share) {
      rqBuilder.withJsonBody(share);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Add Project
   * @remarks Operation handler for addProject
   * @param { SaveProjectRequest } saveProject
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public addProject(saveProject: AbstractSaveProjectRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Project> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/project`);


    if (saveProject) {
      rqBuilder.withJsonBody(saveProject);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get Similar Projects by Area
   * @remarks Operation handler for getSimilarProjectsByArea
   * @param { SimilarProjectsArea } similarProjectsArea
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getSimilarProjectsByArea(similarProjectsArea: SimilarProjectsArea, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<Project>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/project/similar`);


    if (similarProjectsArea) {
      rqBuilder.withJsonBody(similarProjectsArea);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}

import { Project } from '../models/project.model';
import { SaveProjectRequest } from '../models/saveprojectrequest';
import { ShareProviderRequest } from '../models/shareproviderrequest';
import { SimilarProjectsArea } from '../models/similarprojectsarea';
import { AbstractSaveProjectRequest } from '../common/AbstractSaveProjectRequest';
