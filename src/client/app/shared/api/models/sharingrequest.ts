import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class SharingRequest extends BaseDto {
  private _provider: string;
  private _project: string;

  /**
   * Initialize a new instance of SharingRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.provider = input['provider'];
      this.project = input['project'];
  }

  /**
   * Completely clone this instance.
   * @returns SharingRequest - Cloned object.
   **/
  public clone(): SharingRequest {
    return new SharingRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['provider'] = this.provider;
    result['project'] = this.project;
    return result;
  }

  /**
  * Social network provider
  * @returns - Current value of provider.
  **/
  get provider(): string {
    return this._provider;
  }

  /**
  * Change the value of provider.
  * @param newVal - New value to assign.
  **/
  set provider(newVal: string) {
    this._provider = newVal;
  }

  /**
  * Unique identifier of project
  * @returns - Current value of project.
  **/
  get project(): string {
    return this._project;
  }

  /**
  * Change the value of project.
  * @param newVal - New value to assign.
  **/
  set project(newVal: string) {
    this._project = newVal;
  }


}

