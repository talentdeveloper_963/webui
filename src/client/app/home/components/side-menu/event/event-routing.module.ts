import { Routes } from '@angular/router';
import { EventCreateActivityComponent } from './activities/event-create/event-create.activity';
import { EventViewActivityComponent } from './activities/event-view/event-view.component';
import { AuthorizedGuard } from '../../../../shared/guards/authorized.guard';
import { EventResolver } from './event.resolver';

export const eventRoutes: Routes = [
  {path: 'event', component: EventCreateActivityComponent, canActivate: [AuthorizedGuard]},
  {
    path: 'event/:id',
    component: EventViewActivityComponent,
    resolve: {
      project: EventResolver,
    }
  }
];
