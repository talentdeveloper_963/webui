import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class Event extends BaseDto implements AbstractProject {
  private _id: string;
  private _name: string;
  private _description: string;
  private _creator: Author;
  private _start_date: string;
  private _end_date: string;
  private _location: LocationInfo;
  private _capacity: number;
  private _can_vote: boolean;
  private _under_voting: boolean;
  private _won_voting: boolean;
  private _votes_received: number;

  /**
   * Initialize a new instance of Event
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.name = input['name'];
      this.description = input['description'];
      this.creator = input['creator'] ? new Author(input['creator']) : null;
      this.startDate = input['start_date'];
      this.endDate = input['end_date'];
      this.location = input['location'] ? new LocationInfo(input['location']) : null;
      this.capacity = input['capacity'];
      this.canVote = input['can_vote'];
      this.underVoting = input['under_voting'];
      this.wonVoting = input['won_voting'];
      this.votesReceived = input['votes_received'];
  }

  /**
   * Completely clone this instance.
   * @returns Event - Cloned object.
   **/
  public clone(): Event {
    return new Event(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['name'] = this.name;
    result['description'] = this.description;
    result['creator'] = this.creator ? this.creator.toJSON() : null;
    result['start_date'] = this.startDate;
    result['end_date'] = this.endDate;
    result['location'] = this.location ? this.location.toJSON() : null;
    result['capacity'] = this.capacity;
    result['can_vote'] = this.canVote;
    result['under_voting'] = this.underVoting;
    result['won_voting'] = this.wonVoting;
    result['votes_received'] = this.votesReceived;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of name
  * @returns - Current value of name.
  **/
  get name(): string {
    return this._name;
  }

  /**
  * Change the value of name.
  * @param newVal - New value to assign.
  **/
  set name(newVal: string) {
    this._name = newVal;
  }

  /**
  * Get value of description
  * @returns - Current value of description.
  **/
  get description(): string {
    return this._description;
  }

  /**
  * Change the value of description.
  * @param newVal - New value to assign.
  **/
  set description(newVal: string) {
    this._description = newVal;
  }

  /**
  * Get value of creator
  * @returns - Current value of creator.
  **/
  get creator(): Author {
    return this._creator;
  }

  /**
  * Change the value of creator.
  * @param newVal - New value to assign.
  **/
  set creator(newVal: Author) {
    this._creator = newVal;
  }

  /**
  * Get value of start_date
  * @returns - Current value of start_date.
  **/
  get startDate(): string {
    return this._start_date;
  }

  /**
  * Change the value of start_date.
  * @param newVal - New value to assign.
  **/
  set startDate(newVal: string) {
    this._start_date = newVal;
  }

  /**
  * Get value of end_date
  * @returns - Current value of end_date.
  **/
  get endDate(): string {
    return this._end_date;
  }

  /**
  * Change the value of end_date.
  * @param newVal - New value to assign.
  **/
  set endDate(newVal: string) {
    this._end_date = newVal;
  }

  /**
  * Get value of location
  * @returns - Current value of location.
  **/
  get location(): LocationInfo {
    return this._location;
  }

  /**
  * Change the value of location.
  * @param newVal - New value to assign.
  **/
  set location(newVal: LocationInfo) {
    this._location = newVal;
  }

  /**
  * Get value of capacity
  * @returns - Current value of capacity.
  **/
  get capacity(): number {
    return this._capacity;
  }

  /**
  * Change the value of capacity.
  * @param newVal - New value to assign.
  **/
  set capacity(newVal: number) {
    this._capacity = newVal;
  }

  /**
  * Get value of can_vote
  * @returns - Current value of can_vote.
  **/
  get canVote(): boolean {
    return this._can_vote;
  }

  /**
  * Change the value of can_vote.
  * @param newVal - New value to assign.
  **/
  set canVote(newVal: boolean) {
    this._can_vote = newVal;
  }

  /**
  * Get value of under_voting
  * @returns - Current value of under_voting.
  **/
  get underVoting(): boolean {
    return this._under_voting;
  }

  /**
  * Change the value of under_voting.
  * @param newVal - New value to assign.
  **/
  set underVoting(newVal: boolean) {
    this._under_voting = newVal;
  }

  /**
  * Get value of won_voting
  * @returns - Current value of won_voting.
  **/
  get wonVoting(): boolean {
    return this._won_voting;
  }

  /**
  * Change the value of won_voting.
  * @param newVal - New value to assign.
  **/
  set wonVoting(newVal: boolean) {
    this._won_voting = newVal;
  }

  /**
  * Get value of votes_received
  * @returns - Current value of votes_received.
  **/
  get votesReceived(): number {
    return this._votes_received;
  }

  /**
  * Change the value of votes_received.
  * @param newVal - New value to assign.
  **/
  set votesReceived(newVal: number) {
    this._votes_received = newVal;
  }


}

import { Author } from './author';
import { LocationInfo } from './location-info.models';
import { AbstractProject } from '../common/AbstractProject';
