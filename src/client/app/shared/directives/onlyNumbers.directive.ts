import { Directive, HostListener } from '@angular/core';
import { isNavigationButton, isNumberButton, isOnlyNumbersByRegExp } from '../utils/utils';

@Directive({
  selector: '[onlyNumbers]'
})
export class OnlyNumbersDirective {

  constructor() { }

  @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent) {
    const keyCode = e.which || e.keyCode;
    if (isNavigationButton(keyCode, e.ctrlKey, e.metaKey)) {
      return;
    } else {
      if (isNumberButton(keyCode)) {
        return;
      } else {
        e.preventDefault();
      }
    }
  }

  @HostListener('paste', ['$event']) onPaste(event: Event) {
    if ((<ClipboardEvent>event).clipboardData) {
      const value = (<ClipboardEvent>event).clipboardData.getData('text/plain');
      if (!isOnlyNumbersByRegExp(value)) {
        event.preventDefault();
      }
    }
  }

}
