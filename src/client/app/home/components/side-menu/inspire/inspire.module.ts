import { NgModule } from '@angular/core';
import { ButtonModule, DialogModule } from 'primeng/primeng';
import { SharedModule } from '../../../../shared/shared.module';
import { IdeaService } from './services/idea.service';
import { InspireApi } from '../../../../shared/api/service/inspire';
import { InspireFormComponent } from './components/inspire-form/inspire-form.component';
import { InspireCreateActivityComponent } from './activities/inspire-create/inspire-create.activity';
import { TranslateModule } from '@ngx-translate/core';
import { InspireViewActivityComponent } from './activities/inspire-view/inspire-view.activity';
import { IdeaResolver } from './inspire.resolver';

@NgModule({
  declarations: [
    InspireFormComponent,
    InspireCreateActivityComponent,
    InspireViewActivityComponent,
  ],
  imports: [
    SharedModule,
    TranslateModule.forChild(),
    DialogModule,
    ButtonModule,
  ],
  exports: [
    InspireFormComponent,
    InspireCreateActivityComponent,
  ],
  providers: [
    IdeaResolver,
    IdeaService,
    InspireApi,
  ]
})
export class InspireModule {
}
