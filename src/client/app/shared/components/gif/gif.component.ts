import { Component } from '@angular/core';
import { GifService } from '../../services/gif.service';
import { GifTypes } from '../../constants/GifTypes';

@Component({
  moduleId: module.id,
  selector: 'cd-gif',
  templateUrl: 'gif.component.html',
  styleUrls: ['gif.component.css']
})
export class GifComponent {
  public GifTypes = GifTypes;

  constructor(private gifService: GifService) {
  }

  getGifType() {
    return this.gifService.getGifType();
  }

  isVisible() {
    return this.gifService.isVisible();
  }
}
