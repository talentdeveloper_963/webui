import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UserApi } from '../../shared/api/service/user';
import { User } from '../../shared/api/models/user';
import { ApiResponseDto } from '../../shared/api/common/ApiResponseDto';
import { UpdateInfoRequest } from '../../shared/api/models/updateinforequest';
import { SessionService } from '../../shared/services/session.service';
import { LoggingService } from '../../shared/modules/utility/service/logging.service';
import { UpdatePasswordRequest } from '../../shared/api/models/updatepasswordrequest';
import { Project } from '../../shared/api/models/project.model';
import { SendResetPasswordRequest } from '../../shared/api/models/sendresetpasswordrequest';
import { ResetPasswordRequest } from '../../shared/api/models/resetpasswordrequest';
import { Event } from '../../shared/api/models/event.model';
import { Message } from '../../shared/api/models/message';
import { LoadingIndicatorService } from '../../shared/services/loading-indicator.service';
import { CreatePasswordRequest } from '../../shared/api/models/createpasswordrequest';


@Injectable()
export class UserService {

  constructor(
    private userApi: UserApi,
    private logger: LoggingService,
    private loadingIndicator: LoadingIndicatorService,
    private sessionService: SessionService
  ) {}

  /**
   * Query for user
   *
   * @param {string} id - unique id of user to load
   * @returns {Observable<Location>} - promise that will resolve user object
   */
  public getUser(id: string): Observable<User> {
    return this.userApi.getUserById(id)
      .map((response: ApiResponseDto<User>) => {
        return new User(response.payload);
      });
  }

  public updateUserPassword(passwords: UpdatePasswordRequest) {
    return this.userApi.changePassword(passwords)
      .catch(error => Observable.throw(error));
  }

  public updateUserInfo(newInfo: UpdateInfoRequest): Observable<User> {
    return this.userApi.updateInfo(newInfo)
      .map((response: ApiResponseDto<User>) => {
        const user = new User(response.payload);
        this.sessionService.authorize(user);
        return user;
      });
  }

  public getProjectsById(id: string): Observable<Project[]> {
    return this.userApi.getProjectsById(id)
      .map((response: ApiResponseDto<Project[]>) => {
        return response.payload.map((project: Project) => new Project(project));
      })
      .catch(error => Observable.throw(error));
  }

  public getEventsById(id: string): Observable<Event[]> {
    return this.userApi.getEventsById(id)
      .map((response: ApiResponseDto<Event[]>) => {
        return response.payload.map((event: Event) => new Event(event));
      })
      .catch(error => Observable.throw(error));
  }

  public getMessagesById(id: string): Observable<Message[]> {
    return this.userApi.getMessagesById(id)
      .map((response: ApiResponseDto<Message[]>) => {
        return response.payload.map((message: Message) => new Message(message));
      })
      .catch(error => Observable.throw(error));
  }

  public sendResetPassword(email: SendResetPasswordRequest) {
    return this.userApi.sendResetPassword(email)
      .catch(error => Observable.throw(error));
  }

  public resetPassword(token: string, password: ResetPasswordRequest) {
    return this.userApi.resetPassword(token, password)
      .catch(error => Observable.throw(error));
  }

  public createUserPassword(password: CreatePasswordRequest) {
    return this.userApi.createPassword(password)
      .catch(error => Observable.throw(error));
  }
}
