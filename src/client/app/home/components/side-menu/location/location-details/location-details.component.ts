import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { CONSTANTS } from '../../../../../shared/constants/constants';
import { Location } from '../../../../../shared/api/models/location.models';
import { ROUTES } from '../../../../../shared/constants/routes';
import { Subscription } from 'rxjs/Subscription';
import { SizeService } from '../../../../../shared/services/size.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from '../location.service';
import { NavigationService } from '../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../shared/services/state.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../shared/components/notification/notification.service';
import { SessionService } from '../../../../../shared/services/session.service';
import { LoggingService } from '../../../../../shared/modules/utility/service/logging.service';
import { ProjectTypeService } from '../../project/project-type.service';
import { ProjectType } from '../../../../../shared/api/models/project-type.model';

declare namespace DISQUS {
  function reset(a: Object): any;
}

@Component({
  moduleId: module.id,
  selector: 'cd-location-details',
  templateUrl: 'location-details.component.html',
  styleUrls: ['location-details.component.css']
})
export class LocationDetailsComponent implements OnDestroy, OnInit {
  public showLocationModal = true;
  public location: Location;
  public routes = ROUTES;
  public labels = CONSTANTS.LOCATION;
  public toggle = true;
  public dialogPositionTop = 60;
  public dialogPositionLeft = 180;
  public projectType: ProjectType;

  private modalStateChangeSubscription: Subscription;
  private routeDataSubscription: Subscription;
  private widthSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private translate: TranslateService,
              private sizeService: SizeService,
              private sessionService: SessionService,
              private stateService: StateService,
              private cdRef: ChangeDetectorRef,
              private locationService: LocationService,
              private logger: LoggingService,
              private navigationService: NavigationService,
              private notificationService: NotificationService,
              private projectTypeService: ProjectTypeService) {
  }

  public ngOnInit() {
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */

    this.routeDataSubscription = this.route.data.subscribe((data: any) => {
      this.location = data.location;
      this.stateService.selectLocation(this.location.type);
      this.getProjectType();
    });

    const codemos_identifier = this.location.id;
    const codemos_title = this.location.creator.firstName + ' ' + this.location.creator.lastName + ' - ' + this.location.address;

    var d = document, s = d.createElement('script');
    s.src = 'https://codemos.disqus.com/embed.js';
    s.setAttribute('data-timestamp', String(+new Date()));
    (d.head || d.body).appendChild(s);

    setTimeout(function() {
      DISQUS.reset({
        reload: true,
        config: function() {
          this.page.url = window.location.origin + '/' + codemos_identifier;
          this.page.identifier = codemos_identifier;
          this.page.title = codemos_title;
        }
      });
    }, 2000);

    this.modalStateChangeSubscription = this.stateService.locationModalStateChange.subscribe((showLocationModal: boolean) => {
      this.showLocationModal = showLocationModal;
      this.locationService.getLocation(this.location.id)
        .subscribe((updatedLocation: Location) => {
          this.location = updatedLocation;
          this.getProjectType();
        });
      this.cdRef.detectChanges();
    });

    this.widthSubscription = this.sizeService.getMenuWidth().subscribe((menuWidth: number) => {
      // TODO: Identify purpose of magic numbers;
      if (menuWidth) {
        this.dialogPositionLeft = Math.round(menuWidth) + 10;
      }
    });
  }

  public ngOnDestroy() {
    this.routeDataSubscription.unsubscribe();
    this.widthSubscription.unsubscribe();
    this.modalStateChangeSubscription.unsubscribe();
    this.stateService.closeLocation();
  }

  public canEdit(): boolean {
    return !this.location.locked;
  }

  public removeLocation() {
    if (this.sessionService.isAuthorized() && this.sessionService.getSecurityPrincipal().isItMy(this.location)) {
      this.locationService.removeLocation(this.location.id)
        .subscribe(() => {
          this.navigationService.removeLocationMarker(this.location.id);
          this.router.navigate(['../../'], {relativeTo: this.route});
        }, (error) => this.logger.error(error));
    }
  }

  public onHide() {
    // this.location = null;
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  public openProject(id: string) {
    this.stateService.hideLocationModal();
    this.router.navigate([ROUTES.project, id], {relativeTo: this.route});
  }

  public openEvent(id: string) {
    this.stateService.hideLocationModal();
    this.router.navigate([ROUTES.event, id], {relativeTo: this.route});
  }

  public openIdea(id: string) {
    this.stateService.hideLocationModal();
    this.router.navigate([ROUTES.inspire, id], {relativeTo: this.route});
  }

  public doToggle() {
    if (this.sizeService.getWidth() < 900) {
      this.toggle = !this.toggle;
    }
  }

  private getProjectType() {
    this.projectTypeService.getProjectTypeByName(this.location.type)
      .subscribe((projectType: ProjectType) => {
        this.projectType = projectType;
      });
  }
}
