import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Project } from '../../api/models/project.model';
import { Subscription } from 'rxjs/Subscription';
import { SizeService } from '../../services/size.service';

const ProgressBar = require('progressbar.js');


@Component({
  moduleId: module.id,
  selector: 'cd-project-impact',
  templateUrl: 'project-impact.component.html',
  styleUrls: ['project-impact.component.css'],
})
export class ProjectImpactComponent implements OnInit, OnDestroy {
  @Input() project: Project;
  @Input() labels: any;
  @Input() display: boolean;
  @Input() showSubmitToVote: boolean;
  @Input() showVote: boolean;
  @Input() width = 100;
  @Input() height = 200;
  @Input() value = 40;
  @Input() barColor = 'green';
  @Input() bgColor = 'grey';
  @Input() isLocked: boolean;
  @Output() hide: EventEmitter<null> = new EventEmitter<null>();
  @Output() show: EventEmitter<null> = new EventEmitter<null>();
  @Output() share: EventEmitter<null> = new EventEmitter<null>();
  @Output() vote: EventEmitter<null> = new EventEmitter<null>();
  @Output() submitToVote: EventEmitter<null> = new EventEmitter<null>();

  co2Unit = 'kg/year';
  hoursSavedUnit = 'hrs/year';
  showConfirmDialog: boolean;
  positionLeftConfirm: number;
  confirmDialogWidth = 500;
  co2Percentage: number;
  hoursSavedPercentage: number;
  jobsCreatedPercentage: number;
  dialogPositionLeft = 180;
  dialogPositionTop = 60;
  toggle = true;

  private widthSubscription: Subscription;

  constructor(private sizeService: SizeService) { }

  ngOnInit() {
    this.widthSubscription = this.sizeService.getMenuWidth()
      .subscribe((menuWidth: number) => {
        if (menuWidth) {
          this.dialogPositionLeft = Math.round(menuWidth) + 10;
        }
      });
    this.positionLeftConfirm = (window.innerWidth - this.confirmDialogWidth) / 2;
    this.co2Percentage = this.project.performance * 20;
    this.hoursSavedPercentage = this.project.performance * 20;
    this.jobsCreatedPercentage = Math.round((this.project.impact.jobsCreated / 1200) * 10) / 10 * 100;
  }

  ngOnDestroy() {
    if (this.widthSubscription) {
      this.widthSubscription.unsubscribe();
    }
  }

  public formatCo2(value: number) {
    if (value >= 10000) {
      this.co2Unit = 'tons/year';
      return value / 1000;
    }
    this.co2Unit = 'kgs/year';
    return value;
  }

  public formatHoursSaved(value: number) {
    if (value >= 500) {
      this.hoursSavedUnit = 'days/year';
      return Math.round(value / 24);
    }
    this.hoursSavedUnit = 'hrs/year';
    return value;
  }

  public onShow() {
    this.show.emit();
  }

  public onHide() {
    this.hide.emit();
  }

  public onShare() {
    this.share.emit();
  }

  public onVote() {
    this.vote.emit();
  }

  public onSubmitToVote() {
    this.showConfirmDialog = false;
    this.submitToVote.emit();
  }

  public onHideConfirmDialog() {
    this.showConfirmDialog = false;
  }

  public onShowConfirmDialog() {
    this.showConfirmDialog = true;
  }
}
