import { Injectable } from '@angular/core';
import { EventApi } from '../../../../../shared/api/service/event';
import { ApiResponseDto } from '../../../../../shared/api/common/ApiResponseDto';
import { Observable } from 'rxjs/Observable';
import { AbstractProjectService } from '../../../../../shared/services/abstract-project.service';
import { Event } from '../../../../../shared/api/models/event.model';
import { AbstractSaveProjectRequest } from '../../../../../shared/api/common/AbstractSaveProjectRequest';

@Injectable()
export class EventService implements AbstractProjectService<Event> {

  constructor(private eventApi: EventApi) { }

  getProject(eventId: string) {
    return this.eventApi.getProject(eventId)
      .map((response: ApiResponseDto<Event>) => {
        return new Event(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  getMyProjects() {
    return this.eventApi.getMyProjects()
      .map((response: ApiResponseDto<Event[]>) => {
        return response.payload.map((event: Event) => new Event(event));
      })
      .catch(error => Observable.throw(error));
  }

  addProject(event: AbstractSaveProjectRequest) {
    return this.eventApi.addProject(event)
      .map((response: ApiResponseDto<Event>) => {
        return new Event(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  updateProject(id: string, saveEvent: AbstractSaveProjectRequest): Observable<Event> {
    return this.eventApi.updateProject(id, saveEvent)
      .map((response: ApiResponseDto<Event>) => {
        return new Event(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  removeProject(id: string): Observable<any> {
    return this.eventApi.removeProject(id);
  }

  submitToVote(id: string): Observable<Event> {
    return this.eventApi.submitToVote(id)
      .map((response: ApiResponseDto<Event>) => {
        return new Event(response.payload);
      });
  }

  vote(id: string): Observable<Event> {
    return this.eventApi.vote(id)
      .map((response: ApiResponseDto<Event>) => {
        return new Event(response.payload);
      });
  }
}
