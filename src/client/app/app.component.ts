import { Component, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import './operators';
import { LoggingService } from './shared/modules/utility/service/logging.service';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './shared/services/language.service';
import { IdleService } from './shared/services/idle.service';
import { NotificationService } from './shared/components/notification/notification.service';
import { I18N_MISSIONS } from './shared/i18n/elements';
import 'moment/locale/fr';
import 'moment/locale/it';
import 'moment/locale/de';
import 'moment/locale/es';
import 'moment/locale/ru';
import { SessionService } from './shared/services/session.service';
import { CordovaService } from './shared/services/cordova.service';
import { Subscription } from 'rxjs/Subscription';
import { LoadingIndicatorService } from './shared/services/loading-indicator.service';

/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  public showWarning: boolean;
  public isBusy = false;

  private _loadingSubscription: Subscription;

  constructor(private logger: LoggingService,
              private translate: TranslateService,
              private language: LanguageService,
              private idleService: IdleService,
              private notificationService: NotificationService,
              private loadingIndicatorService: LoadingIndicatorService,
              private sessionService: SessionService,
              private cordovaService: CordovaService) {

    if (this.cordovaService.isCordova()) {
      document.addEventListener('deviceready', () => {
        (<any>navigator).splashscreen.hide();
        this.showWarning = !this.cordovaService.isConnectionEstablished();
      }, false);

      document.addEventListener('offline', () => {
        this.showWarning = true;
      }, false);

      document.addEventListener('online', () => {
        this.showWarning = false;
      }, false);

      document.addEventListener('backbutton', (event: Event) => {
        event.preventDefault();
        event.stopPropagation();
        if (!this.cordovaService.isConnectionEstablished()) {
          (<any>navigator).app.exitApp();
        } else {
          (<any>navigator).app.backHistory();
        }
      }, false);
    }

    // Prevent user scale in Safari
    document.addEventListener('gesturestart', (event: Event) => {
      event.preventDefault();
    }, false);

    document.addEventListener('dblclick', (event: Event) => {
      event.preventDefault();
    }, false);
    // End prevent user scale in Safari

    this.translate.setDefaultLang('en');
    this.translate.onLangChange
      .subscribe((language: any) => moment.locale(language.lang));
    const lang = this.language.getSelectedLanguage();
    if (lang) {
      this.translate.use(lang);
    }

    this.idleService.startTimer(() => {
      // Prevent show messages if no Internet connection for Cordova application
      if (this.cordovaService.isCordova() && !this.cordovaService.isConnectionEstablished()) {
        return;
      }
      this.notificationService.show(I18N_MISSIONS[Math.floor(Math.random() * I18N_MISSIONS.length)]);
    }, 20000);
  }

  public ngOnInit() {
    this._loadingSubscription = this.loadingIndicatorService.busy.subscribe((isBusy) => {
      this.isBusy = isBusy;
    });
  }

  public ngOnDestroy() {
    this._loadingSubscription.unsubscribe();
  }
}
