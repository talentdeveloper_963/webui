import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserSignupRequest } from '../../../shared/api/models/usersignuprequest';
import { GENDERS } from '../../constants/genders';
import { VALIDATIONS } from '../../constants/validations';
import { LoggingService } from '../../../shared/modules/utility/service/logging.service';
import { AuthService } from '../../services/auth.service';
import { fromSnakeCaseToCamelCase } from '../../../shared/utils/objUtils';
import { I18N_ERROR_MESSAGES, I18N_GENDERS } from '../../../shared/i18n/elements';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import {
  containsWhiteSpacesOnly,
  controlsMatch,
  hasAtLeastOneUpper,
  hasntWhiteSpaces,
  validEmail
} from '../../helpers/validators';
import { ROUTES } from '../../../shared/constants/routes';
import { getValidatorsFromValidationsObject } from '../../../shared/utils/validationUtils';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';


@Component({
  moduleId: module.id,
  selector: 'cd-register-form',
  templateUrl: 'register-form.component.html',
  styleUrls: ['register-form.component.css']
})
export class RegisterFormComponent extends BaseLockComponent implements OnInit {
  public genders = GENDERS;
  public registerForm: FormGroup;
  public emailMustBeUnique: string;
  public VALIDATIONS = VALIDATIONS;
  public ROUTES = ROUTES;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private logger: LoggingService,
    private translate: TranslateService
  ) {
    super();
    this.renderGenders();
  }

  public ngOnInit() {
    this.buildRegistrationForm();
    this.registerForm.valueChanges.subscribe(() => this.emailMustBeUnique = null);
    this.translate.onLangChange
      .subscribe(() => {
        this.renderGenders();
      });
  }

  public onSubmit(e: Event) {
    e.preventDefault();
    if (this.registerForm.valid) {
      this.lockComponent();
      const correctFormatCredentials = Object.assign(this.registerForm.value, { language: this.translate.currentLang, useNotifications: true });
      const signupCredentials: UserSignupRequest = new UserSignupRequest(correctFormatCredentials);
      signupCredentials.language = this.translate.currentLang;
      this.authService.registerUser(signupCredentials)
        .finally(() => this.unlockComponent())
        .subscribe(null, error => {
          if (error.errorCode === 4) {
            this.translate.get(I18N_ERROR_MESSAGES.emailMustBeUnique)
              .subscribe((message) => {
                this.emailMustBeUnique = message;
              });
          }

          const validationErrors = fromSnakeCaseToCamelCase(error.validationErrors);
          for (const field in validationErrors) {
            if (validationErrors.hasOwnProperty(field)) {
              this.registerForm.get(field).setErrors({serverError: validationErrors[field]});
            }
          }
        });
    }
  }

  private renderGenders() {
    this.translate.get(_.values(I18N_GENDERS))
      .subscribe(res => {
        this.genders = this.genders.map(gender => {
          gender.text = res[I18N_GENDERS[gender.value]];
          return gender;
        });
      });
  }

  private buildRegistrationForm() {
    this.registerForm = this.fb.group({
      firstName: ['', [
        VALIDATIONS.firstName.required.value && Validators.required,
        Validators.maxLength(VALIDATIONS.firstName.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      lastName: ['', [
        VALIDATIONS.lastName.required.value && Validators.required,
        Validators.maxLength(VALIDATIONS.lastName.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      email: ['', [
        VALIDATIONS.email.required.value && Validators.required,
        Validators.maxLength(VALIDATIONS.email.maxLength.value),
        validEmail,
      ]],
      password: ['', [
        VALIDATIONS.password.required.value && Validators.required,
        Validators.minLength(VALIDATIONS.password.minLength.value),
        Validators.maxLength(VALIDATIONS.password.maxLength.value),
        hasntWhiteSpaces,
        hasAtLeastOneUpper,
      ]],
      confirm: ['', [
        VALIDATIONS.confirmPassword.required.value && Validators.required
      ]],
      birthDate: ['', getValidatorsFromValidationsObject(VALIDATIONS.birthDate)],
      gender: ['', []],
      city: ['', [
        Validators.maxLength(VALIDATIONS.city.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      address: ['', [
        Validators.maxLength(VALIDATIONS.address.maxLength.value),
      ]],
      terms: [false, [
        Validators.requiredTrue,
      ]]
    }, {
      validator: controlsMatch('password', 'confirm'),
    });
  }
}
