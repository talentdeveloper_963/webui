import { Injectable } from '@angular/core';
import { Project } from '../api/models/project.model';
import { Event } from '../api/models/event.model';
import { Message } from '../api/models/message';
import { AbstractProject } from '../api/common/AbstractProject';
import { PROJECT_TYPES } from '../constants/projectTypes';
import { SessionService } from './session.service';

@Injectable()
export class ConditionService {
  constructor(
    private sessionService: SessionService,
  ) {}

  public getProjectTypeString(project: AbstractProject): string {
    if (project instanceof Project) {
      return PROJECT_TYPES.PUBLIC_PROJECT;
    } else if (project instanceof Event) {
      return PROJECT_TYPES.EVENT;
    } else if (project instanceof Message) {
      return PROJECT_TYPES.IDEA;
    }
    return null;
  }

  public getProjectTypeRoute(project: AbstractProject): string {
    if (project instanceof Project) {
      return PROJECT_TYPES.PUBLIC_PROJECT;
    } else if (project instanceof Event) {
      return PROJECT_TYPES.EVENT;
    } else if (project instanceof Message) {
      return PROJECT_TYPES.IDEA;
    }
    return null;
  }

  public isUserCreator(project: AbstractProject): boolean {
    if (this.sessionService.isAuthorized()) {
      return this.sessionService.getSecurityPrincipal().isItMy(project);
    } else {
      return false;
    }
  }

  public canAbstractProjectBeSubmittedToVote(project: AbstractProject): boolean {
    if (project instanceof Event) {
      return this.canEventBeSubmittedToVote(project);
    } else if (project instanceof Project) {
      return this.canProjectBeSubmittedToVote(project);
    } else if (project instanceof Message) {
      return this.canIdeaBeSubmittedToVote(project);
    }
    return false;
  }

  private canProjectBeSubmittedToVote(project: Project): boolean {
    return !project.underVoting && !project.wonVoting && project.performance >= 1 && this.isUserCreator(project);
  }

  private canEventBeSubmittedToVote(event: Event): boolean {
    return !event.underVoting && !event.wonVoting && this.isUserCreator(event);
  }

  private canIdeaBeSubmittedToVote(idea: Message): boolean {
    return !idea.underVoting && !idea.wonVoting && this.isUserCreator(idea);
  }

}
