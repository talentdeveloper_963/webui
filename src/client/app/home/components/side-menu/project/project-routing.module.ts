import { Routes } from '@angular/router';
import { ProjectViewActivityComponent } from './activities/project-view/project-view.activity';
import { ProjectCreateActivityComponent } from './activities/project-create/project-create.activity';
import { AuthorizedGuard } from '../../../../shared/guards/authorized.guard';
import { ProjectResolver } from './project.resolver';

export const projectRoutes: Routes = [
  {
    path: 'project',
    component: ProjectCreateActivityComponent,
    canActivate: [AuthorizedGuard]
  },
  {
    path: 'project/:id',
    component: ProjectViewActivityComponent,
    resolve: {
      project: ProjectResolver
    }
  },
];
