import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class VotingApi extends BaseApi {


  /**
   * Get nearest submitted to vote project
   * @remarks Operation handler for getNearestVoting
   * @param { FindNearestVotingProjectRequest } visitedProjects  
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getNearestVoting(visitedProjects: FindNearestVotingProjectRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<VotingProject> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/voting/nearest`);

    
    if (visitedProjects) {
      rqBuilder.withJsonBody(visitedProjects);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}

import { FindNearestVotingProjectRequest } from '../models/findnearestvotingprojectrequest';
import { VotingProject } from '../models/votingproject';
