import { Component, NgZone, OnInit } from '@angular/core';
import { AuthService as SocialAuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular5-social-login';
import { socialProviders } from '../../constants/socialProviders';
import { AuthService } from '../../services/auth.service';
import { CordovaService } from '../../../shared/services/cordova.service';
import { LoggingService } from '../../../shared/modules/utility/service/logging.service';
import { TwitterLoginProvider } from '../../services/twitter.login.provider';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';
import { OAuthToken } from '../../../shared/api/models/oAuthToken';

@Component({
  moduleId: module.id,
  selector: 'cd-social-buttons',
  templateUrl: 'social-buttons.component.html',
  styleUrls: ['social-buttons.component.css']
})

export class SocialButtonsComponent extends BaseLockComponent implements OnInit {
  public socialProviders = socialProviders;
  public isCordova: boolean;

  constructor(
    private socialAuthService: SocialAuthService,
    private authService: AuthService,
    private cordovaService: CordovaService,
    private logger: LoggingService,
    private twitterLoginProvider: TwitterLoginProvider,
    private zone: NgZone
  ) {
    super();
  }

  public ngOnInit() {
    this.isCordova = this.cordovaService.isCordova();
  }

  public socialSignIn(socialProvider: number) {
    if (!this.isCordova) {
      if (socialProvider === socialProviders.FACEBOOK) {
        this.browserSocialSignIn(FacebookLoginProvider.PROVIDER_ID, socialProvider);
      } else if (socialProvider === socialProviders.GOOGLE) {
        this.browserSocialSignIn(GoogleLoginProvider.PROVIDER_ID, socialProvider);
      } else if (socialProvider === socialProviders.TWITTER) {
        this.browserTwitterSocialSignIn();
      }

    } else if (this.isCordova) {
      if (socialProvider === socialProviders.GOOGLE) {
        this.cordovaGoogleSocialSignIn();
      } else if (socialProvider === socialProviders.FACEBOOK) {
        this.cordovaFacebookSocialSignIn();
      }
    }
  }

  private browserSocialSignIn(socialPlatformProvider: string, socialProvider: number) {
    this.socialAuthService.signIn(socialPlatformProvider)
      .then((userData: any) => {
        this.lockComponent();
        this.authService.sendSocialRequest(socialProvider, userData)
          .finally(() => {
            this.unlockComponent();
          })
          .subscribe();
      });
  }

  private cordovaGoogleSocialSignIn() {
    this.lockComponent();
    (window as any).plugins.googleplus.login({},
      (res: any) => {
        this.authService.sendSocialRequest(this.socialProviders.GOOGLE, {token: res.accessToken})
          .finally(() => {
            this.unlockComponent();
          })
          .subscribe();
      },
      (msg: any) => {
        this.zone.run(() => this.unlockComponent());
        this.logger.log('error: ' + msg);
      }
    );
  }

  private cordovaFacebookSocialSignIn() {
    this.lockComponent();
    facebookConnectPlugin.login(['email', 'public_profile'],
      (res: any) => {
        this.authService.sendSocialRequest(this.socialProviders.FACEBOOK, {token: res.authResponse.accessToken})
          .finally(() => {
            this.unlockComponent();
          })
          .subscribe();
      },
      (msg: any) => {
        this.zone.run(() => this.unlockComponent());
        this.logger.log(msg);
      });
  }

  private browserTwitterSocialSignIn() {
    this.twitterLoginProvider.signIn()
      .subscribe((data: OAuthToken) => {
        this.lockComponent();
        this.authService.sendSocialRequest(socialProviders.TWITTER, {token: data})
          .finally(() => {
            this.unlockComponent();
          })
          .subscribe();
      });
  }

}
