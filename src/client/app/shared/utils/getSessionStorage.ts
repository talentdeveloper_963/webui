export const getSessionStorage = () => {
  let storageImpl: any = null;

  try {
    window.sessionStorage.setItem('storage', '');
    window.sessionStorage.removeItem('storage');
    storageImpl = window.sessionStorage;
  } catch (err) {
    storageImpl = new SessionStorageAlternative();
  }
  return storageImpl;
};

class SessionStorageAlternative {
  private structureSessionStorage: any = {};

  setItem(key: string, value: any) {
    this.structureSessionStorage[key] = value;
  }

  getItem(key: string) {
    if (typeof this.structureSessionStorage[key] !== 'undefined' ) {
      return this.structureSessionStorage[key];
    } else {
      return null;
    }
  }

  removeItem(key: string) {
    this.structureSessionStorage[key] = undefined;
  }
}


