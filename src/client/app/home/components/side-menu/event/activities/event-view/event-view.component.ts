import { Component, OnDestroy } from '@angular/core';
import { LocationService } from '../../../location/location.service';
import { Event } from '../../../../../../shared/api/models/event.model';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { AbstractProjectViewComponent } from '../../../../../../shared/components/project-view/project-view.component';
import { EventService } from '../../services/event.service';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { StateService } from '../../../../../../shared/services/state.service';
import { SessionService } from '../../../../../../shared/services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { ProjectTypeService } from '../../../project/project-type.service';

@Component({
  moduleId: module.id,
  selector: 'cd-event-view',
  templateUrl: 'event-view.component.html'
})
export class EventViewActivityComponent extends AbstractProjectViewComponent<Event> implements OnDestroy {

  public badRequest: string;

  public LABELS = CONSTANTS.EVENT.VIEW;

  constructor(
    protected eventService: EventService,
    protected loadingIndicatorService: LoadingIndicatorService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected navigationService: NavigationService,
    protected locationService: LocationService,
    protected stateService: StateService,
    protected promoteThemService: PromoteThemService,
    protected sessionService: SessionService,
    protected translate: TranslateService,
    protected notificationService: NotificationService,
    protected projectTypeService: ProjectTypeService
  ) {
    super(
      eventService,
      locationService,
      loadingIndicatorService,
      router,
      route,
      promoteThemService,
      stateService,
      navigationService,
      sessionService,
      translate,
      notificationService,
      projectTypeService
    );
  }

  public removeProject() {
    super.removeProject('event');
  }

  public nextProject() {
    super.nextProject(PROJECT_TYPES.EVENT);
  }

  public isVisible() {
    return super.isVisible();
  }

  public onHide() {
    super.onHide();
  }

  public onShow() {
    super.onShow();
  }

  public cancel() {
    return super.cancel();
  }
}
