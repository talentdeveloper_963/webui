import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class CreatePasswordRequest extends BaseDto {
  private _password: string;

  /**
   * Initialize a new instance of CreatePasswordRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.password = input['password'];
  }

  /**
   * Completely clone this instance.
   * @returns CreatePasswordRequest - Cloned object.
   **/
  public clone(): CreatePasswordRequest {
    return new CreatePasswordRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['password'] = this.password;
    return result;
  }

  /**
  * Get value of password
  * @returns - Current value of password.
  **/
  get password(): string {
    return this._password;
  }

  /**
  * Change the value of password.
  * @param newVal - New value to assign.
  **/
  set password(newVal: string) {
    this._password = newVal;
  }


}

