import Config from '../../config';
import { clean } from '../../utils';

/**
 * Executes the build process, cleaning all files within the `/cordova/www` directory.
 */
export = clean(Config.CORDOVA_WWW);
