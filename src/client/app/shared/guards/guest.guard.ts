import { CanActivate, Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { SessionService } from '../services/session.service';
import { ROUTES } from '../constants/routes';
import { LoggingService } from '../modules/utility/service/logging.service';
import { ERROR_MESSAGES } from '../constants/errors';

@Injectable()
export class GuestGuard implements CanActivate {

  constructor(private sessionService: SessionService,
              private router: Router,
              private logger: LoggingService,
              private zone: NgZone) { }

  canActivate() {
    if (this.sessionService.isAuthorized()) {
      this.zone.run(() => {
        this.router.navigate([ROUTES.home])
          .then(() => this.logger.log('navigated to home'))
          .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
      });
    }
    return !this.sessionService.isAuthorized();
  }
}
