import { EnvConfig } from './env-config.interface';

const DevConfig: EnvConfig = {
  ENV: 'DEV',
  API: 'http://api-ci.codemos.logicify.com',
  UI_URL: 'http://localhost:5555',
  FAKE_API: 'http://127.0.0.1:3000',
  FACEBOOK_CLIENT_ID: '2057711861138146',
  FACEBOOK_APP_NAME: 'Codemos_dev',
  GOOGLE_CLIENT_ID: '727565445929-cg85pl40itsmfap8o0u3g81bvav7a3g6.apps.googleusercontent.com',
  ANDROID: {
    GOOGLE_CLIENT_ID: '727565445929-cg85pl40itsmfap8o0u3g81bvav7a3g6.apps.googleusercontent.com',
    GOOGLE_REVERSED_CLIENT_ID: 'com.googleusercontent.apps.727565445929-cg85pl40itsmfap8o0u3g81bvav7a3g6'
  },
  IOS: {
    GOOGLE_CLIENT_ID: '727565445929-fip4nagv052mfjmap1gr1j4fb3uce5jt.apps.googleusercontent.com',
    GOOGLE_REVERSED_CLIENT_ID: 'com.googleusercontent.apps.727565445929-fip4nagv052mfjmap1gr1j4fb3uce5jt'
  }
};

export = DevConfig;
