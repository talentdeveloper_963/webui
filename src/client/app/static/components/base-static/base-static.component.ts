import { ROUTES } from '../../../shared/constants/routes';
import { OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';
import { UserPrincipal } from '../../../shared/models/SecurityPrincipal';

export class BaseStaticComponent implements OnInit {
  public ROUTES = ROUTES;

  constructor(
    private translate: TranslateService,
    private sessionService: SessionService,
  ) {}

  ngOnInit() {
    if (this.sessionService.getSecurityPrincipal()) {
      const language = (<UserPrincipal>this.sessionService.getSecurityPrincipal()).user.language;
      if (language) {
        this.translate.use(language);
      }
    }
  }
}
