// Feel free to extend this interface
// depending on your app specific config.
export interface MapboxSettings {
  TOKEN: string;
  STYLE: string;
  MIN_ZOOM: number;
  MAX_ZOOM: number;
}

export interface CordovaGoogleSettings {
  GOOGLE_CLIENT_ID: string;
  GOOGLE_REVERSED_CLIENT_ID: string;
}

export interface EnvConfig {
  UI_URL?: string;
  API?: string;
  FAKE_API?: string;
  ENV?: string;
  VERSION?: string;
  FACEBOOK_CLIENT_ID?: string;
  FACEBOOK_APP_NAME?: string;
  GOOGLE_CLIENT_ID?: string;
  ANDROID?: CordovaGoogleSettings;
  IOS?: CordovaGoogleSettings;
  NAVIGATOR_SETTINGS?: MapboxSettings;
}
