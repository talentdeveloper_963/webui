import { OnDestroy, OnInit } from '@angular/core';
import { NavigationService } from '../../map/navigation.service';

export class AbstractCreatePlaceholderComponent implements OnInit, OnDestroy {
  constructor(private navigationService: NavigationService) {}

  ngOnInit() {
    this.navigationService.setClickHandler();
  }

  ngOnDestroy() {
    this.navigationService.removeClickHandler();
  }
}
