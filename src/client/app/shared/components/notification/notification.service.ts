import { NotificationType } from './notification-type.service';
import { AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs/Subscription';

export class NotificationService implements AfterViewChecked {
  private display = false;
  private message: string;
  private notificationType: NotificationType;
  private subscription: Subscription;

  ngAfterViewChecked() {
    this.notificationType = NotificationType.NOTIFY;
  }

  show(message: string, notificationType?: NotificationType, time = 5000) {
    this.message = message;
    this.display = true;
    this.notificationType = notificationType || NotificationType.NOTIFY;
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = Observable.interval(time)
      .subscribe(() => {
        this.hide();
      });
  }

  hide() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.display = false;
    this.message = null;
  }

  isVisible(): boolean {
    return this.display;
  }

  getMessage(): string {
    return this.message;
  }

  getNotificationType(): NotificationType {
    return this.notificationType;
  }
}
