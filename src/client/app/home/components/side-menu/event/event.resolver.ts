import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Event } from '../../../../shared/api/models/event.model';
import { EventService } from './services/event.service';

@Injectable()
export class EventResolver implements Resolve<Event> {
  constructor(private eventService: EventService) {
  }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Event> {
    return this.eventService.getProject(route.params.id);
  }
}
