import { Injectable } from '@angular/core';

@Injectable()
export class LoggingService {
  private isProd: boolean;

  constructor() {
    this.isProd = String('<%= BUILD_TYPE %>') === 'prod';
  }

  public log(message?: any, ...optionalParams: any[]): void {
    console.log(message, ...optionalParams);
  }

  public debug(message?: any, ...optionalParams: any[]): void {
    if (!this.isProd) {
      console.log(message, ...optionalParams);
    }
  }

  public error(message?: any, ...optionalParams: any[]): void {
    console.error(message, ...optionalParams);
  }
}
