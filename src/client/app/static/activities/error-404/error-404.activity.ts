import { Component } from '@angular/core';
import { BaseStaticComponent } from '../../components/base-static/base-static.component';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  moduleId: module.id,
  selector: 'cd-error404',
  templateUrl: 'error-404.activity.html',
  styleUrls: ['error-404.activity.css'],
})
export class Error404ActivityComponent extends BaseStaticComponent {
  constructor(
    translate: TranslateService,
    sessionService: SessionService,
  ) {
    super(translate, sessionService);
  }
}
