import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { COOKIES } from '../constants/cookieNames';

@Injectable()
export class CookieService {
  constructor(
    @Inject( DOCUMENT ) private document: any
  ) {
    this.set(COOKIES.visitedWelcomePage, false);
  }

  public set(key: string, value: any) {
    this.document.cookie = `${key}=${value}`;
  }

  public get(key: string): string {
    const encodedKey = encodeURIComponent(key);
    const regExp: RegExp = CookieService.getCookieRegExp(encodedKey);
    const result: RegExpExecArray = regExp.exec(this.document.cookie);

    return JSON.parse(decodeURIComponent(result[1]));
  }

  private static getCookieRegExp(name: string): RegExp {
    const escapedName: string = name.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/ig, '\\$1');
    return new RegExp('(?:^' + escapedName + '|;\\s*' + escapedName + ')=(.*?)(?:;|$)', 'g');
  }
}
