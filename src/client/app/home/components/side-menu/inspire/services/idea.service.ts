import { Injectable } from '@angular/core';
import { ApiResponseDto } from '../../../../../shared/api/common/ApiResponseDto';
import { Observable } from 'rxjs/Observable';
import { AbstractProjectService } from '../../../../../shared/services/abstract-project.service';
import { Message } from '../../../../../shared/api/models/message';
import { InspireApi } from '../../../../../shared/api/service/inspire';
import { FindNearestInspirationalMessageRequest } from '../../../../../shared/api/models/findnearestinspirationalmessagerequest';
import { AbstractSaveProjectRequest } from '../../../../../shared/api/common/AbstractSaveProjectRequest';

@Injectable()
export class IdeaService implements AbstractProjectService<Message> {

  constructor(private inspireApi: InspireApi) {
  }

  public getProject(messageId: string) {
    return this.inspireApi.getProject(messageId)
      .map((response: ApiResponseDto<Message>) => {
        return new Message(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  public getMyProjects() {
    return this.inspireApi.getMyProjects()
      .map((response: ApiResponseDto<Message[]>) => {
        return response.payload.map((message: Message) => new Message(message));
      })
      .catch(error => Observable.throw(error));
  }

  public addProject(message: AbstractSaveProjectRequest) {
    return this.inspireApi.addProject(message)
      .map((response: ApiResponseDto<Message>) => {
        return new Message(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  public updateProject(id: string, saveMessage: AbstractSaveProjectRequest): Observable<Message> {
    return this.inspireApi.updateProject(id, saveMessage)
      .map((response: ApiResponseDto<Message>) => {
        return new Message(response.payload);
      })
      .catch(error => Observable.throw(error));
  }

  public removeProject(id: string): Observable<any> {
    return this.inspireApi.removeProject(id)
      .catch(error => Observable.throw(error));
  }

  public getNearestMessage(visitedMessages: FindNearestInspirationalMessageRequest) {
    return this.inspireApi.getNearestInspirationalMessage(visitedMessages)
      .map((response: ApiResponseDto<Message>) => {
        if (response.payload) {
          return new Message(response.payload);
        } else {
          return null;
        }
      })
      .catch(error => Observable.throw(error));
  }

  public submitToVote(id: string): Observable<Message> {
    return this.inspireApi.submitToVote(id)
      .map((response: ApiResponseDto<Message>) => {
        return new Message(response.payload);
      });
  }

  public vote(id: string): Observable<Message> {
    return this.inspireApi.vote(id)
      .map((response: ApiResponseDto<Message>) => {
        return new Message(response.payload);
      });
  }
}
