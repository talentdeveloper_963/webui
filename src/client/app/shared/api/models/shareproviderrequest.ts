import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class ShareProviderRequest extends BaseDto {
  private _provider: string;

  /**
   * Initialize a new instance of ShareProviderRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.provider = input['provider'];
  }

  /**
   * Completely clone this instance.
   * @returns ShareProviderRequest - Cloned object.
   **/
  public clone(): ShareProviderRequest {
    return new ShareProviderRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['provider'] = this.provider;
    return result;
  }

  /**
  * Social network provider
  * @returns - Current value of provider.
  **/
  get provider(): string {
    return this._provider;
  }

  /**
  * Change the value of provider.
  * @param newVal - New value to assign.
  **/
  set provider(newVal: string) {
    this._provider = newVal;
  }


}

