import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';
import { LoginCredentialsRequest } from '../models/logincredentialsrequest';
import { User } from '../models/user';
import { UserSignupRequest } from '../models/usersignuprequest';
import { SocialRequest } from '../models/socialRequest';
import { OAuth1SocialRequest } from '../models/oAuth1SocialRequest';

@Injectable()
export class AuthApi extends BaseApi {


  /**
   * Authenticate user by email/password
   * @remarks Operation handler for login
   * @param { LoginCredentialsRequest } loginCredentials
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   * @param { boolean } strictAllow - should response be done without method check
   **/
  public login(loginCredentials: LoginCredentialsRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('post')
      .withPath(`/auth/login`);


    if (loginCredentials) {
      rqBuilder.withJsonBody(loginCredentials);
    }
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }


  /**
   * Logout user
   * @remarks Operation handler for logout

   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public logout(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('delete')
      .withPath(`/auth/logout`);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Register User
   * @remarks Operation handler for signup
   * @param { UserSignupRequest } userSignup
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   * @param { boolean } strictAllow - should response be done without method check
   **/
  public signup(userSignup: UserSignupRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('post')
      .withPath(`/user/register`);


    if (userSignup) {
      rqBuilder.withJsonBody(userSignup);
    }
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }


  /**
   * Get session
   * @remarks Operation handler for session

   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public session(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('get')
      .withPath(`/auth/session`);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

  /**
   * Send social provider token
   * @param { string } providerName - name social provider
   * @param { SocialRequest  | OAuth1SocialRequest } socialRequest - social provider auth token
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   * @param { boolean } strictAllow - should response be done without method check
   **/
  public sendSocialRequest(providerName: string, socialRequest: SocialRequest | OAuth1SocialRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('post')
      .withPath(`/auth/social/${providerName}/complete`);
    if (socialRequest) {
      rqBuilder.withJsonBody(socialRequest);
    }
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }

  /**
   * Get OAuth1 request token
   * @param { string } providerName - name social provider
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   * @param { boolean } strictAllow - should response be done without method check
   **/
  public getRequestToken(providerName: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('get')
      .withPath(`/auth/social/${providerName}/request_token`);
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }

  /**
   * Get OAuth1 Access token
   * @param { string } providerName - name social provider
   * @param { OAuth1SocialRequest } oAuthAccessTokenRequest - social provider auth token
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   * @param { boolean } strictAllow - should response be done without method check
   **/
  public getAccessToken(providerName: string, oAuthAccessTokenRequest: OAuth1SocialRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('post')
      .withPath(`/auth/social/${providerName}/access_token`);
    if (oAuthAccessTokenRequest) {
      rqBuilder.withJsonBody(oAuthAccessTokenRequest);
    }
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }
}

