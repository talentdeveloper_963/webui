import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';
import { AbstractSaveProjectRequest } from '../common/AbstractSaveProjectRequest';


export class SaveEventRequest extends BaseDto implements AbstractSaveProjectRequest {
  private _name: string;
  private _description: string;
  private _start_date: string;
  private _end_date: string;
  private _location: string;
  private _capacity: number;

  /**
   * Initialize a new instance of SaveEventRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.name = input['name'];
      this.description = input['description'];
      this.startDate = input['start_date'];
      this.endDate = input['end_date'];
      this.location = input['location'];
      this.capacity = input['capacity'];
  }

  /**
   * Completely clone this instance.
   * @returns SaveEventRequest - Cloned object.
   **/
  public clone(): SaveEventRequest {
    return new SaveEventRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['name'] = this.name;
    result['description'] = this.description;
    result['start_date'] = this.startDate;
    result['end_date'] = this.endDate;
    result['location'] = this.location;
    result['capacity'] = this.capacity;
    return result;
  }

  /**
  * Get value of name
  * @returns - Current value of name.
  **/
  get name(): string {
    return this._name;
  }

  /**
  * Change the value of name.
  * @param newVal - New value to assign.
  **/
  set name(newVal: string) {
    this._name = newVal;
  }

  /**
  * Get value of description
  * @returns - Current value of description.
  **/
  get description(): string {
    return this._description;
  }

  /**
  * Change the value of description.
  * @param newVal - New value to assign.
  **/
  set description(newVal: string) {
    this._description = newVal;
  }

  /**
  * Get value of start_date
  * @returns - Current value of start_date.
  **/
  get startDate(): string {
    return this._start_date;
  }

  /**
  * Change the value of start_date.
  * @param newVal - New value to assign.
  **/
  set startDate(newVal: string) {
    this._start_date = newVal;
  }

  /**
  * Get value of end_date
  * @returns - Current value of end_date.
  **/
  get endDate(): string {
    return this._end_date;
  }

  /**
  * Change the value of end_date.
  * @param newVal - New value to assign.
  **/
  set endDate(newVal: string) {
    this._end_date = newVal;
  }

  /**
  * Get value of location
  * @returns - Current value of location.
  **/
  get location(): string {
    return this._location;
  }

  /**
  * Change the value of location.
  * @param newVal - New value to assign.
  **/
  set location(newVal: string) {
    this._location = newVal;
  }

  /**
  * Get value of capacity
  * @returns - Current value of capacity.
  **/
  get capacity(): number {
    return this._capacity;
  }

  /**
  * Change the value of capacity.
  * @param newVal - New value to assign.
  **/
  set capacity(newVal: number) {
    this._capacity = newVal;
  }


}

