import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class Author extends BaseDto {
  private _id: string;
  private _first_name: string;
  private _last_name: string;

  /**
   * Initialize a new instance of Author
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.id = input['id'];
      this.firstName = input['first_name'];
      this.lastName = input['last_name'];
  }

  /**
   * Completely clone this instance.
   * @returns Author - Cloned object.
   **/
  public clone(): Author {
    return new Author(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['id'] = this.id;
    result['first_name'] = this.firstName;
    result['last_name'] = this.lastName;
    return result;
  }

  /**
  * Get value of id
  * @returns - Current value of id.
  **/
  get id(): string {
    return this._id;
  }

  /**
  * Change the value of id.
  * @param newVal - New value to assign.
  **/
  set id(newVal: string) {
    this._id = newVal;
  }

  /**
  * Get value of first_name
  * @returns - Current value of first_name.
  **/
  get firstName(): string {
    return this._first_name;
  }

  /**
  * Change the value of first_name.
  * @param newVal - New value to assign.
  **/
  set firstName(newVal: string) {
    this._first_name = newVal;
  }

  /**
  * Get value of last_name
  * @returns - Current value of last_name.
  **/
  get lastName(): string {
    return this._last_name;
  }

  /**
  * Change the value of last_name.
  * @param newVal - New value to assign.
  **/
  set lastName(newVal: string) {
    this._last_name = newVal;
  }


}

