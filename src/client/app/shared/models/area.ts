import { Distance } from '../constants/distance';


export class Area {
  private _latitude: number;
  private _longitude: number;
  private _radius: number;

  constructor(latitude: number, longitude: number, radius: number = Distance.TWENTY_KILOMETERS) {
    this._latitude = latitude;
    this._longitude = longitude;
    this._radius = radius;
  }

  get radius(): number {
    return this._radius;
  }

  set radius(value: number) {
    this._radius = value;
  }

  get latitude(): number {
    return this._latitude;
  }

  get longitude(): number {
    return this._longitude;
  }
}
