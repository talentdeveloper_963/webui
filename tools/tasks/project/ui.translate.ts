import * as gulp from 'gulp';
import { join } from 'path';

import Config from '../../config';
import TranslateUtils from '../../utils/project/translate_utils';
import * as _ from 'lodash';

import * as jeditor from 'gulp-json-editor';

/**
 * This task takes newPhrases file and translate them to 6 languages. Each new translation will be added to files.
 */
export = () => {
  const languages = ['en', 'fr', 'ru', 'de', 'it', 'es'];
  _.each(languages, (lang: string) => {
    const filePath = join(Config.APP_SRC, '/assets/i18n/' + lang + '.json');
    TranslateUtils.translate(lang)
      .then((translate: any) => {
        gulp.src(filePath, {base: './'})
          .pipe(jeditor(function (json: any) {
            return _.defaults(json, translate);
          }))
          .pipe(gulp.dest('./'));
      });
  });
};
