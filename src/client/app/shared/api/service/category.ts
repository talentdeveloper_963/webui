import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';
import { ProjectCategory } from '../models/project-category.model';

@Injectable()
export class CategoryApi extends BaseApi {


  /**
   * Get categories which are not mapped with resources
   * @remarks Operation handler for getCategories

   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getCategories(extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<ProjectCategory>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/category`);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}
