import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Project } from '../../../../../../shared/api/models/project.model';
import { ProjectService } from '../../project.service';
import { SaveProjectRequest } from '../../../../../../shared/api/models/saveprojectrequest';
import { LoadingIndicatorService } from '../../../../../../shared/services/loading-indicator.service';
import { LocationService } from '../../../location/location.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../../../shared/components/notification/notification.service';
import { PromoteThemService } from '../../../promote/promote-them/promote-them.service';
import { ROUTES } from '../../../../../../shared/constants/routes';
import { CONSTANTS } from '../../../../../../shared/constants/constants';
import { VALIDATIONS } from '../../../../../../auth/constants/validations';
import { TranslateService } from '@ngx-translate/core';
import { getValidatorsFromValidationsObject } from '../../../../../../shared/utils/validationUtils';
import { AbstractProjectFormComponent } from '../../../../../../shared/components/project-form/project-form.component';
import { ProjectCategory } from '../../../../../../shared/api/models/project-category.model';
import { SessionService } from '../../../../../../shared/services/session.service';
import { PROJECT_TYPES } from '../../../../../../shared/constants/projectTypes';
import { SizeService } from '../../../../../../shared/services/size.service';
import { ConditionService } from '../../../../../../shared/services/condition.service';
import { NavigationService } from '../../../../../../shared/map/navigation.service';
import { ProjectType } from '../../../../../../shared/api/models/project-type.model';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';

@Component({
  moduleId: module.id,
  selector: 'cd-project-form',
  templateUrl: 'project-form.component.html',
  styleUrls: ['project-form.component.css']
})
export class ProjectFormComponent extends AbstractProjectFormComponent<Project, SaveProjectRequest> implements OnChanges {

  @Input()
  public subcategoryError: string;

  @Input()
  public projectType: ProjectType;

  @Input()
  public categories: Array<ProjectCategory> = [];

  @Input() isLocked: boolean;

  public isIE11: boolean;

  showProjectDialog = true;
  showImpactDialog = false;
  VALIDATIONS = VALIDATIONS;
  projectUpdateForm: FormGroup;
  ROUTES = ROUTES;
  LABELS = CONSTANTS.PROJECT;
  confirmDialogWidth = 500;
  positionLeft: number;
  positionLeftConfirm: number;
  dialogClass = 'project-dialog';
  private _coefficient = 1200;
  public translatedProjectCategories: ProjectCategory[];
  private _translation: Subscription;
  private _changeLang: Subscription;

  constructor(protected projectService: ProjectService,
              protected conditionService: ConditionService,
              loadingIndicatorService: LoadingIndicatorService,
              locationService: LocationService,
              router: Router,
              route: ActivatedRoute,
              sessionService: SessionService,
              sizeService: SizeService,
              navigationService: NavigationService,
              protected promoteThemService: PromoteThemService,
              protected notificationService: NotificationService,
              private fb: FormBuilder,
              protected translate: TranslateService,
              private windowService: SizeService) {
    super(
      promoteThemService,
      projectService,
      locationService,
      loadingIndicatorService,
      translate,
      notificationService,
      sessionService,
      sizeService,
      conditionService,
      navigationService
    );
    this.isIE11 = !!(window as any).MSInputMethodContext && !!(document as any).documentMode;
    this.positionLeft = window.innerWidth / 2;
    this.positionLeftConfirm = (window.innerWidth - this.confirmDialogWidth) / 2;
  }

  protected onInit() {
    this._changeLang = this.translate.onLangChange
      .subscribe(() => {
        this.translateProjectCategories();
      });
  }

  protected onDestroy() {
    if (this._translation) {
      this._translation.unsubscribe();
    }
    if (this._changeLang) {
      this._changeLang.unsubscribe();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.categories) {
      this.translateProjectCategories();
    }
  }

  public onHide() {
    super.onHide();
  }

  public onShowImpact() {
    this.showProjectDialog = false;
    this.showImpactDialog = true;
  }

  public onHideImpact() {
    this.showImpactDialog = false;
    this.showProjectDialog = true;
  }

  public isUserCreator(): boolean {
    return super.isUserCreator();
  }

  public canBeModified(): boolean {
    return super.canBeModified();
  }

  public isVotesReceivedVisible(): boolean {
    return this.projectUpdateForm && this.project.underVoting && this.isUserCreator() || !this.isUserCreator();
  }

  public canProjectBeVoted(): boolean {
    return super.canProjectBeVoted();
  }

  public canProjectBeSubmittedToVote(): boolean {
    return super.canProjectBeSubmittedToVote();
  }

  public submitToVote() {
    return super.submitToVote();
  }

  public vote() {
    return super.vote();
  }

  public removeProject() {
    super.removeProject();
  }

  public nextProject() {
    super.nextProject();
  }

  public onSubmit() {
    return super.onSubmit();
  }

  public share() {
    super.share(PROJECT_TYPES.PUBLIC_PROJECT);
  }

  public initForm() {
    this.projectUpdateForm = this.fb.group({
      location: [this.project.location],
      name: [this.project.name, getValidatorsFromValidationsObject(VALIDATIONS.project.name)],
      description: [this.project.description, getValidatorsFromValidationsObject(VALIDATIONS.project.description)],
      capacity: [this.project.capacity, getValidatorsFromValidationsObject(VALIDATIONS.project.capacity)],
      category: [this.project.category, getValidatorsFromValidationsObject(VALIDATIONS.project.category)]
    });
    this.projectUpdateForm.get('category').valueChanges
      .subscribe(() => this.subcategoryError = null);
  }

  public buildSaveRequest(): any {
    return new SaveProjectRequest({
      name: this.projectUpdateForm.get('name').value || '',
      description: this.projectUpdateForm.get('description').value || '',
      capacity: this.projectUpdateForm.get('capacity').value,
      location: this.project.location.id,
      category: this.projectUpdateForm.get('category').value.id,
    });
  }

  public onCancel() {
    if (this.editMode && !this.createMode) {
      this.projectService.getProject(this.project.id)
        .subscribe((project: Project) => {
          this.project = project;
        });
    }
    return super.onCancel();
  }

  public equals(c1: ProjectCategory, c2: ProjectCategory): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

  private translateProjectCategories() {
    const categoriesNames = _.map(this.categories, item => item.name);
    this._translation = this.translate.get(categoriesNames)
      .subscribe(res => {
        this.translatedProjectCategories = this.categories.map(item => {
          return new ProjectCategory({
            id: item.id,
            name: res[item.name],
            type: item.type
          });
        });
        this.translatedProjectCategories = _.orderBy(this.translatedProjectCategories, ['name']);
      });
  }

}
