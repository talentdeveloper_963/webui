import { Component, OnInit } from '@angular/core';
import { controlsMatch } from '../../../auth/helpers/validators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionService } from '../../../shared/services/session.service';
import { AuthService } from '../../../auth/services/auth.service';
import { ProjectService } from '../../components/side-menu/project/project.service';
import { LoggingService } from '../../../shared/modules/utility/service/logging.service';
import { Router } from '@angular/router';
import { GENDERS } from '../../../auth/constants/genders';
import { Project } from '../../../shared/api/models/project.model';
import { SecurityPrincipal } from '../../../shared/models/SecurityPrincipal';
import { VALIDATIONS } from '../../../auth/constants/validations';
import { ROUTES } from '../../../shared/constants/routes';
import { UpdatePasswordRequest } from '../../../shared/api/models/updatepasswordrequest';
import { ERROR_MESSAGES } from '../../../shared/constants/errors';
import { UpdateInfoRequest } from '../../../shared/api/models/updateinforequest';
import { UserService } from '../../services/user.service';
import { CONSTANTS } from '../../../shared/constants/constants';
import { I18N_ERROR_MESSAGES } from '../../../shared/i18n/elements';
import { TranslateService } from '@ngx-translate/core';
import { LANGUAGES } from '../../../auth/constants/languages';
import { LanguageService } from '../../../shared/services/language.service';
import {
  containsWhiteSpacesOnly,
  dateIsntGreaterThanToday,
  hasAtLeastOneUpper,
  hasntWhiteSpaces
} from '../../../auth/helpers/validators';
import { EventService } from '../../components/side-menu/event/services/event.service';
import { IdeaService } from '../../components/side-menu/inspire/services/idea.service';
import { Event } from '../../../shared/api/models/event.model';
import { Message } from '../../../shared/api/models/message';
import { PROJECT_TYPES } from '../../../shared/constants/projectTypes';
import { CreatePasswordRequest } from '../../../shared/api/models/createpasswordrequest';
import { Camera } from '../../../shared/models/camera';
import { NavigationService } from '../../../shared/map/navigation.service';
import { BaseLockComponent } from '../../../shared/utils/baseLockComponent';

@Component({
  moduleId: module.id,
  selector: 'cd-profile-edit',
  templateUrl: 'profile-edit.activity.html',
  styleUrls: ['profile-edit.activity.css'],
})

export class ProfileEditActivityComponent extends BaseLockComponent implements OnInit {
  public languages = LANGUAGES;
  public genders = GENDERS;
  public securityPrincipal: SecurityPrincipal = null;
  public projects: Project[] = [];
  public events: Event[] = [];
  public ideas: Message[] = [];
  public displayPasswordDialog = false;
  public displayUpdateDialog = false;
  public displayNotificationsDialog = false;
  public updateForm: FormGroup;
  public passwordForm: FormGroup;
  public notificationsForm: FormGroup;
  public VALIDATIONS = VALIDATIONS;
  public ROUTES = ROUTES;
  public passwordErrorMessage: string = null;
  public LABELS = CONSTANTS.PROFILE_EDIT;
  public showProfileDialog = true;

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionService,
    private authService: AuthService,
    private userService: UserService,
    private projectService: ProjectService,
    private eventService: EventService,
    private inspireService: IdeaService,
    private logger: LoggingService,
    private router: Router,
    private translate: TranslateService,
    private navigationService: NavigationService,
    private languageService: LanguageService
  ) {
    super();
  }

  public ngOnInit() {
    this.securityPrincipal = this.sessionService.getSecurityPrincipal();
    this.getProjects();
    this.buildUpdateForm();
    this.buildPasswordForm();
    this.buildNotificationsForm();
  }

  public getProjects(projectType?: string) {
    // TODO Think about how we can do this better
    if (projectType === PROJECT_TYPES.PUBLIC_PROJECT) {
      this.projectService.getMyProjects().subscribe(projects => {
        this.projects = projects;
      });
    } else if (projectType === PROJECT_TYPES.EVENT) {
      this.eventService.getMyProjects().subscribe(events => {
        this.events = events;
      });
    } else if (projectType === PROJECT_TYPES.IDEA) {
      this.inspireService.getMyProjects().subscribe(messages => {
        this.ideas = messages;
      });
    } else {
      this.projectService.getMyProjects().subscribe(projects => {
        this.projects = projects;
      });

      this.eventService.getMyProjects().subscribe(events => {
        this.events = events;
      });

      this.inspireService.getMyProjects().subscribe(messages => {
        this.ideas = messages;
      });
    }
  }

  public showUpdateDialog() {
    this.displayUpdateDialog = true;
  }

  public showPasswordDialog() {
    this.displayPasswordDialog = true;
  }

  public showNotificationsDialog() {
    this.displayNotificationsDialog = true;
  }

  public onSubmitUpdate() {
    this.lockComponent();
    const correctFormatCredentials = Object.assign(this.securityPrincipal.getPrincipalJSON(), this.updateForm.value);
    const updateInfoRequest = new UpdateInfoRequest(correctFormatCredentials);
    this.userService.updateUserInfo(updateInfoRequest)
      .finally(() => this.unlockComponent())
      .subscribe(() => {
        this.displayUpdateDialog = false;
        this.securityPrincipal = this.sessionService.getSecurityPrincipal();
        this.languageService.setLanguage(this.securityPrincipal.getPrincipalLanguage());
      });
  }

  public onSubmitPassword(event: any) {
    this.lockComponent();
    event.preventDefault();

    if (this.securityPrincipal.getPrincipalHasPasswordField()) {
      const passwords = new UpdatePasswordRequest(this.passwordForm.value);
      this.userService.updateUserPassword(passwords)
        .finally(() => this.unlockComponent())
        .subscribe((res => {
          this.displayPasswordDialog = false;
          this.sessionService.dropSession();
          this.router.navigate([ROUTES.login])
            .then(() => this.logger.log('navigated to login'))
            .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
        }), error => {
          if (error.errorCode === 3) {
            this.translate.get(I18N_ERROR_MESSAGES.passwordDoesNotMatch)
              .subscribe((message) => {
                this.passwordErrorMessage = message;
              });
          }
        });
    } else {
      const newPassword = new CreatePasswordRequest({'password': this.passwordForm.get('newPassword').value});
      this.userService.createUserPassword(newPassword)
        .subscribe(res => {
          this.displayPasswordDialog = false;
          this.sessionService.dropSession();
          this.router.navigate([ROUTES.login])
            .then(() => this.logger.log('navigated to login'))
            .catch((err) => this.logger.error(ERROR_MESSAGES.NAV_ERROR));
        }, error => {
          this.logger.error(error);
        });
    }

  }

  public onSubmitNotifications(event: any) {
    this.lockComponent();
    event.preventDefault();
    const newInfo = new UpdateInfoRequest(Object.assign(this.securityPrincipal.getPrincipalJSON(), this.notificationsForm.value));

    this.userService.updateUserInfo(newInfo)
      .finally(() => this.unlockComponent())
      .subscribe(res => {
        this.securityPrincipal = this.sessionService.getSecurityPrincipal();
        this.displayNotificationsDialog = false;
      });
  }

  public onUpdateDialogHide() {
    this.displayUpdateDialog = false;
    this.updateForm.reset();
  }

  public onUpdateDialogShow() {
    this.displayPasswordDialog = false;
    this.displayNotificationsDialog = false;

    this.buildUpdateForm();
  }

  public onPasswordDialogHide() {
    this.displayPasswordDialog = false;
    this.passwordForm.reset();
  }

  public onPasswordDialogShow() {
    this.displayUpdateDialog = false;
    this.displayNotificationsDialog = false;

    this.buildPasswordForm();
  }

  public onNotificationsDialogHide() {
    this.displayNotificationsDialog = false;
    this.notificationsForm.reset();
  }

  public onNotificationsDialogShow() {
    this.displayUpdateDialog = false;
    this.displayPasswordDialog = false;

    this.buildNotificationsForm();
  }

  public onShow() {
    this.showProfileDialog = true;
  }

  public onHide() {
    this.showProfileDialog = false;
    const camera = this.navigationService.getCameraPosition();
    this.router.navigate([
      ROUTES.camera,
      Camera.toFormatted(camera.latitude),
      Camera.toFormatted(camera.longitude),
      Camera.toFormatted(camera.zoom),
    ]);
  }

  private buildUpdateForm() {
    let defaultDate = null;
    if (this.securityPrincipal.getPrincipalBirthDate()) {
      defaultDate = {
        date: {
          year: new Date(this.securityPrincipal.getPrincipalBirthDate()).getFullYear(),
          month: new Date(this.securityPrincipal.getPrincipalBirthDate()).getMonth() + 1,
          day: new Date(this.securityPrincipal.getPrincipalBirthDate()).getDate(),
        },
        jsdate: new Date(this.securityPrincipal.getPrincipalBirthDate())
      };
    }
    this.updateForm = this.fb.group({
      firstName: [this.securityPrincipal.getPrincipalFirstName(), [
        VALIDATIONS.firstName.required.value && Validators.required,
        Validators.maxLength(VALIDATIONS.firstName.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      lastName: [this.securityPrincipal.getPrincipalLastName(), [
        VALIDATIONS.lastName.required.value && Validators.required,
        Validators.maxLength(VALIDATIONS.lastName.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      birthDate: [this.securityPrincipal.getPrincipalBirthDate() ? new Date(this.securityPrincipal.getPrincipalBirthDate()) : '', [
        dateIsntGreaterThanToday,
      ]],
      gender: [this.securityPrincipal.getPrincipalGender(), []],
      city: [this.securityPrincipal.getPrincipalCity(), [
        Validators.maxLength(VALIDATIONS.city.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      address: [this.securityPrincipal.getPrincipalAddress(), [
        Validators.maxLength(this.VALIDATIONS.address.maxLength.value),
        containsWhiteSpacesOnly,
      ]],
      language: [this.securityPrincipal.getPrincipalLanguage(), []],
    });
  }

  private buildPasswordForm() {
    this.passwordForm = this.fb.group({
      newPassword: ['', [
        VALIDATIONS.newPassword.required.value && Validators.required,
        Validators.minLength(VALIDATIONS.password.minLength.value),
        Validators.maxLength(VALIDATIONS.password.maxLength.value),
        hasntWhiteSpaces,
        hasAtLeastOneUpper,
      ]],
      confirm: ['', [
        VALIDATIONS.confirmPassword.required.value && Validators.required,
      ]],
    }, {
      validator: controlsMatch('newPassword', 'confirm'),
    });

    if (this.securityPrincipal.getPrincipalHasPasswordField()) {
      const passwordField = new FormControl('', [
        VALIDATIONS.password.required.value && Validators.required,
        Validators.minLength(VALIDATIONS.password.minLength.value),
        Validators.maxLength(VALIDATIONS.password.maxLength.value),
        hasntWhiteSpaces,
        hasAtLeastOneUpper,
      ]);
      this.passwordForm.addControl('password', passwordField);
    }

    this.passwordForm.valueChanges.subscribe(() => this.passwordErrorMessage = null);
  }

  private buildNotificationsForm() {
    this.notificationsForm = this.fb.group({
      useNotifications: [this.securityPrincipal.getPrincipalUseNotificationsField(), [VALIDATIONS.notifications.required.value && Validators.required]]
    });
  }
}
