import { BaseApi } from '../common/BaseApi';
import { ApiResponseObservable } from '../common/ApiResponseDto';
import { ApiRequestBuilder } from '../common/ApiRequestBuilder';
import { Dictionary } from '../../types';
import { Injectable } from '@angular/core';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class UserApi extends BaseApi {


  /**
   * Update user&#x27;s information
   * @remarks Operation handler for updateInfo
   * @param { UpdateInfoRequest } newInfo
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public updateInfo(newInfo: UpdateInfoRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('put')
                  .withPath(`/user/profile`);


    if (newInfo) {
      rqBuilder.withJsonBody(newInfo);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get user&#x27;s information by ID
   * @remarks Operation handler for getUserById
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getUserById(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<User> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/user/profile/${id}`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Change user&#x27;s password
   * @remarks Operation handler for changePassword
   * @param { UpdatePasswordRequest } passwords
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public changePassword(passwords: UpdatePasswordRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('put')
                  .withPath(`/user/password`);


    if (passwords) {
      rqBuilder.withJsonBody(passwords);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Send reset password email
   * @remarks Operation handler for sendResetPassword
   * @param { SendResetPasswordRequest } email
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public sendResetPassword(email: SendResetPasswordRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('post')
                  .withPath(`/user/password/reset`);


    if (email) {
      rqBuilder.withJsonBody(email);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }


  /**
   * Reset password
   * @remarks Operation handler for resetPassword
   * @param { string } token  * @param { ResetPasswordRequest } password
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public resetPassword(token: string, password: ResetPasswordRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('put')
                  .withPath(`/user/password/reset/${token}`);

    pathParams['token'] = String(token);


    if (password) {
      rqBuilder.withJsonBody(password);
    }
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }

  /**
   * Create password
   * @remarks Operation handler for createPassword
   * @param { CreatePasswordRequest } password
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public createPassword(password: CreatePasswordRequest, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType, strictAllow = true): ApiResponseObservable<any> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
      .withMethod('post')
      .withPath(`/user/password`);

    if (password) {
      rqBuilder.withJsonBody(password);
    }
    rqBuilder.withPathParams(pathParams)
      .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request, true, strictAllow);
  }

  /**
   * Get user&#x27;s projects by his ID
   * @remarks Operation handler for getProjectsById
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getProjectsById(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<Project>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/user/${id}/projects`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

  /**
   * Get user&#x27;s events by his ID
   * @remarks Operation handler for getEventsById
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getEventsById(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<Event>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/user/${id}/events`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }


  /**
   * Get user&#x27;s messages by his ID
   * @remarks Operation handler for getMessagesById
   * @param { string } id
   * @param { Dictionary<string> } extraHeaders - additional HTTP headers to be added to HTTP request
   * @param { ResponseContentType } responseType - override default response content type
   **/
  public getMessagesById(id: string, extraHeaders?: Dictionary<string>, responseType?: ResponseContentType): ApiResponseObservable<Array<Message>> {
    const queryParams: Dictionary<string> = {};
    const pathParams: Dictionary<string> = {};
    const rqBuilder = new ApiRequestBuilder(this.apiBaseUrl)
                  .withMethod('get')
                  .withPath(`/user/${id}/messages`);

    pathParams['id'] = String(id);
    rqBuilder.withPathParams(pathParams)
             .withQueryParams(queryParams);
    if (extraHeaders) {
      rqBuilder.withExtraHeaders(extraHeaders);
    }
    if (responseType) {
      rqBuilder.withResponseType(responseType);
    }
    const request = rqBuilder.build();
    return this.performRequest(request);
  }

}

import { UpdateInfoRequest } from '../models/updateinforequest';
import { User } from '../models/user';
import { UpdatePasswordRequest } from '../models/updatepasswordrequest';
import { SendResetPasswordRequest } from '../models/sendresetpasswordrequest';
import { ResetPasswordRequest } from '../models/resetpasswordrequest';
import { Project } from '../models/project.model';
import { Event } from '../models/event.model';
import { Message } from '../models/message';
import { CreatePasswordRequest } from '../models/createpasswordrequest';
