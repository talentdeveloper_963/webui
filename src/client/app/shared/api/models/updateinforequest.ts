import { BaseDto } from '../common/BaseDto';
import { Dictionary } from '../../types';


export class UpdateInfoRequest extends BaseDto {
  private _first_name: string;
  private _last_name: string;
  private _birth_date: string;
  private _gender: string;
  private _city: string;
  private _language: string;
  private _address: string;
  private _use_notifications: boolean;

  /**
   * Initialize a new instance of UpdateInfoRequest
   * @param {object}    input     - Initial data to be set into the model fields. Should be an object representing JSON.
   **/
   constructor(input?: any) {
      super();
      // Skip if no input
      if (!input) {
        return;
      }
      input = super.fromCamelCaseToSnakeCase(input);
      this.firstName = input['first_name'];
      this.lastName = input['last_name'];
      this.birthDate = input['birth_date'] ? input['birth_date'] : null;
      this.gender = input['gender'];
      this.city = input['city'];
      this.language = input['language'];
      this.address = input['address'];
      this.useNotifications = input['use_notifications'];
  }

  /**
   * Completely clone this instance.
   * @returns UpdateInfoRequest - Cloned object.
   **/
  public clone(): UpdateInfoRequest {
    return new UpdateInfoRequest(this.toJSON());
  }

  public toJSON(): Dictionary<any> {
    const result: Dictionary<any> = {};
    result['first_name'] = this.firstName;
    result['last_name'] = this.lastName;
    result['birth_date'] = this.birthDate;
    result['gender'] = this.gender;
    result['city'] = this.city;
    result['language'] = this.language;
    result['address'] = this.address;
    result['use_notifications'] = this.useNotifications;
    return result;
  }

  /**
  * Get value of first_name
  * @returns - Current value of first_name.
  **/
  get firstName(): string {
    return this._first_name;
  }

  /**
  * Change the value of first_name.
  * @param newVal - New value to assign.
  **/
  set firstName(newVal: string) {
    this._first_name = newVal;
  }

  /**
  * Get value of last_name
  * @returns - Current value of last_name.
  **/
  get lastName(): string {
    return this._last_name;
  }

  /**
  * Change the value of last_name.
  * @param newVal - New value to assign.
  **/
  set lastName(newVal: string) {
    this._last_name = newVal;
  }

  /**
  * Get value of birth_date
  * @returns - Current value of birth_date.
  **/
  get birthDate(): string {
    return this._birth_date;
  }

  /**
  * Change the value of birth_date.
  * @param newVal - New value to assign.
  **/
  set birthDate(newVal: string) {
    this._birth_date = newVal;
  }

  /**
  * Get value of gender
  * @returns - Current value of gender.
  **/
  get gender(): string {
    return this._gender;
  }

  /**
  * Change the value of gender.
  * @param newVal - New value to assign.
  **/
  set gender(newVal: string) {
    this._gender = newVal;
  }

  /**
  * Get value of City
  * @returns - Current value of City.
  **/
  get city(): string {
    return this._city;
  }

  /**
  * Change the value of City.
  * @param newVal - New value to assign.
  **/
  set city(newVal: string) {
    this._city = newVal;
  }

  /**
  * Get value of language
  * @returns - Current value of language.
  **/
  get language(): string {
    return this._language;
  }

  /**
  * Change the value of language.
  * @param newVal - New value to assign.
  **/
  set language(newVal: string) {
    this._language = newVal;
  }

  /**
  * Get value of address
  * @returns - Current value of address.
  **/
  get address(): string {
    return this._address;
  }

  /**
  * Change the value of address.
  * @param newVal - New value to assign.
  **/
  set address(newVal: string) {
    this._address = newVal;
  }

  /**
  * Get value of use_notifications
  * @returns - Current value of use_notifications.
  **/
  get useNotifications(): boolean {
    return this._use_notifications;
  }

  /**
  * Change the value of use_notifications.
  * @param newVal - New value to assign.
  **/
  set useNotifications(newVal: boolean) {
    this._use_notifications = newVal;
  }


}

