import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseApi } from '../../shared/api/common/BaseApi';
import { ApiRequestBuilder } from '../../shared/api/common/ApiRequestBuilder';

@Injectable()
export class ApiMockService extends BaseApi {
  private baseApiUrl = 'https://reqres.in';

  public makeDelayedRequest(delaySeconds: number): Observable<any> {
    const request = new ApiRequestBuilder(this.baseApiUrl)
      .withMethod('get')
      .withPath(`/api/users?delay=${delaySeconds}`)
      .build();

    return this.performRequest(request, false, true);
  }
}
