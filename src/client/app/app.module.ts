import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StaticModule } from './static/static.module';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IdleService } from './shared/services/idle.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { NavigationService } from './shared/map/navigation.service';
import { MapboxService } from './shared/map/mapbox/mapbox.service';
import { AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule, } from 'angular5-social-login';
import { Config } from './shared/config/env.config';
import { TranslateHttpLoader } from './shared/i18n/translate-http.loader';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, ['./assets/i18n/', './assets/i18n/calendar/']);
}

export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(Config.FACEBOOK_CLIENT_ID)
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(Config.GOOGLE_CLIENT_ID)
      },
    ]);
  return config;
}

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    NgSelectModule,
    HttpClientModule,
    AppRoutingModule,
    HomeModule,
    AuthModule,
    StaticModule,
    SharedModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    SocialLoginModule
  ],
  declarations: [AppComponent],
  exports: [RouterModule],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: '/' /*TODO: APP_BASE is not interpolated with  <%= APP_BASE %>*/
    },
    {
      provide: NavigationService,
      useClass: MapboxService
    },
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    IdleService
  ],
  bootstrap: [AppComponent]

})
export class AppModule {
}

