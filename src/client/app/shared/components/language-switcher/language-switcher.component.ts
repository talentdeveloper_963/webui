import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../services/language.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../../home/services/user.service';
import { UpdateInfoRequest } from '../../api/models/updateinforequest';
import { SessionService } from '../../services/session.service';
import { UserPrincipal } from '../../models/SecurityPrincipal';
import { LANGUAGES } from '../../../auth/constants/languages';
import { User } from '../../api/models/user';

@Component({
  moduleId: module.id,
  selector: 'cd-language-switcher',
  templateUrl: 'language-switcher.component.html',
  styleUrls: ['language-switcher.component.css']
})
export class LanguageSwitcherComponent implements OnInit {
  selectedLanguage: string;
  languages = LANGUAGES;

  constructor (
    private languageService: LanguageService,
    private translate: TranslateService,
    private userService: UserService,
    private sessionService: SessionService,
  ) {}

  ngOnInit() {
    this.languageService.languageChanged.subscribe((language: string) => {
      this.selectedLanguage = language;
    });
  }

  changeLanguage(e: any) {
    const langCode = e.value;
    this.selectedLanguage = langCode;
    if (this.sessionService.isAuthorized()) {
      const updatedUser = new User((<UserPrincipal>this.sessionService.getSecurityPrincipal()).user);
      updatedUser.language = langCode;
      const updateInfoRequest = new UpdateInfoRequest(updatedUser.toJSON());
      this.userService.updateUserInfo(updateInfoRequest)
        .subscribe();
    }
    this.languageService.setLanguage(langCode);
  }
}
