import { EnvConfig } from './env-config.interface';

const ProdConfig: EnvConfig = {
  ENV: 'PROD',
  API: 'https://api.codemos.world',
  UI_URL: 'https://www.codemos.world',
  FACEBOOK_CLIENT_ID: '386508058507534',
  FACEBOOK_APP_NAME: 'Codemos',
  GOOGLE_CLIENT_ID: '757677612049-24hp6nk67oal3hatijff1ldluek7e3r8.apps.googleusercontent.com',
  ANDROID: {
    GOOGLE_CLIENT_ID: '757677612049-24hp6nk67oal3hatijff1ldluek7e3r8.apps.googleusercontent.com',
    GOOGLE_REVERSED_CLIENT_ID: 'com.googleusercontent.apps.757677612049-24hp6nk67oal3hatijff1ldluek7e3r8',
  },
  IOS: {
    GOOGLE_CLIENT_ID: '757677612049-51d22mv1n62moige8202h7q8gqla2eo4.apps.googleusercontent.com',
    GOOGLE_REVERSED_CLIENT_ID: 'com.googleusercontent.apps.757677612049-51d22mv1n62moige8202h7q8gqla2eo4',
  },
  NAVIGATOR_SETTINGS: {
    TOKEN: 'pk.eyJ1IjoianVsaWF0YWRkZWkiLCJhIjoiY2ppYnVqd2ttMDQ0dzNyczc2bGpmcGF5bSJ9.bfHXYICa0gwZHmXjIxiNPA',
    STYLE: 'mapbox://styles/mapbox/streets-v10',
    MIN_ZOOM: 1,
    MAX_ZOOM: 20
  }
};

export = ProdConfig;

