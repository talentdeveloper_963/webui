import { Component } from '@angular/core';
import { NotificationService } from './notification.service';
import { NotificationType } from './notification-type.service';
import { Router } from '@angular/router';
import { ROUTES } from '../../constants/routes';

@Component({
  moduleId: module.id,
  selector: 'cd-notification',
  templateUrl: 'notification.component.html',
  styleUrls: ['notification.component.css']
})
export class NotificationComponent {
  public notificationType: NotificationType;

  constructor(
    private notificationService: NotificationService,
    private router: Router,
  ) {}

  getMessage() {
    this.notificationType = this.notificationService.getNotificationType();
    return this.notificationService.getMessage();
  }

  isVisible() {
    return this.notificationService.isVisible();
  }

  goToPartnersPage() {
    this.router.navigate([ROUTES.partners]);
  }
}
